import numpy as np
import plotly as py
import plotly.graph_objs as go
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Find best masking strategy, and updating it after every batch of new reads.')
parser.add_argument("-file", help="Name (possibly with path) of the simulation output file to look at. Should end with errWeird0.1 or similar . ",default="/Desktop/ReadUntil_nanopore/simulations/simulationsOutput/N50_mu3_rho3.0_alpha3.0_lam10.0_sd1.0_theta0.5_delRatio0.4_e0.001_errD0.001_errWeird0.001_nPeaks1.0_covRatio10.0")
parser.add_argument("-html", help="should I make html plots?", action="store_true")
parser.add_argument("-coverage", help="should I make a coverage plot instead?", action="store_true")
parser.add_argument("-MLST", help="should I add to coverage plot the regions of interest?", action="store_true")
parser.add_argument("-nReplicates", help="Number of files to look for (if some are missing, it still runs on the remaining ones)", type=int, default=31)
parser.add_argument("-normalization", help="normalize average coverage by this number", type=float, default=1.0)
parser.add_argument("-shortening", help="Number of kb in one point of the coverage plot", type=int, default=4)
parser.add_argument("-compareStrat", help="should I compare different RU strategies?", action="store_true")
parser.add_argument("-yeast", help="Yeast simulations?", action="store_true")

args = parser.parse_args()
fileName=args.file
nReps=args.nReplicates
shortening=args.shortening
normalization=args.normalization

if args.coverage and (not args.compareStrat):
	dfRU=[]
	for i in range(nReps):
		try:
			#arr=np.genfromtxt(fname = fileName+"_run"+str(i+1)+"_coverageRU.csv", dtype = int)
			#print(arr)
			#dfRU.append(arr)
			file=open(fileName+"_run"+str(i+1)+"_coverageRU.csv")
			line=file.readline()
			line=line.replace(",\n","")
			arr=np.asarray(line.split(","),dtype=float)
			#dfRU.append(line.split(","))
			file.close()
			#print(arr)
			#print(arr.shape)
			sal=int(len(arr)/shortening)
			arrShort=np.zeros(sal)
			for j in range(sal):
				arrShort[j]=np.mean(arr[j*shortening:(j+1)*shortening])
			dfRU.append(arrShort)
				
		except FileNotFoundError:
			continue
	N=len(dfRU[0])
	print(len(dfRU))
	print(N)
	dfNaive=[]
	for i in range(nReps):
		try:
			file=open(fileName+"_run"+str(i+1)+"_coverageNaive.csv")
			line=file.readline()
			line=line.replace(",\n","")
			arr=np.asarray(line.split(","),dtype=float)
			file.close()
			sal=int(len(arr)/shortening)
			arrShort=np.zeros(sal)
			for j in range(sal):
				arrShort[j]=np.mean(arr[j*shortening:(j+1)*shortening])
			dfNaive.append(arrShort)
		except FileNotFoundError:
			continue
	print(len(dfNaive))	
	foundInterest=False
	if args.MLST:
		i=1
		while (not foundInterest) and i<=n:
			try:
				file=open(fileName+"_run"+str(i)+"_interestingRegions.csv")
				line=file.readline()
				line=line.replace(",\n","")
				dfInterest=np.asarray(line.split(","),dtype=int)
				#dfInterest=line.split(",")
				file.close()
				foundInterest=True
				print(len(dfInterest))
			except FileNotFoundError:
				i+=1
	# for i in range(N):
# 		for j in range(len(dfRU)):
# 			dfRU[j][i]=int(dfRU[j][i])
# 			dfNaive[j][i]=int(dfNaive[j][i])
# 		if args.MLST:
# 			dfInterest[i]=int(dfInterest[i])




elif args.coverage and args.compareStrat:
	descriptions=["batch4000,fullRU","batch500,fullRU","batch4000,halfRU","batch500,halfRU","batch4000,quarterRU","batch500,quarterRU"]
	names=["_batchSize4000_timeFor1stUpdate1.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate1.0_strategyEvery1000000","_batchSize4000_timeFor1stUpdate100000000.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate100000000.0_strategyEvery1000000","_batchSize4000_timeFor1stUpdate150000000.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate150000000.0_strategyEvery1000000"]
	if args.yeast:
		names=["_batchSize4000_timeFor1stUpdate1.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate1.0_strategyEvery3000000","_batchSize4000_timeFor1stUpdate200000000.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate200000000.0_strategyEvery3000000","_batchSize4000_timeFor1stUpdate400000000.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate400000000.0_strategyEvery3000000"]
		descriptions=["batch 4000, full RU","batch 500, full RU","batch 4000, 2/3 RU","batch 500, 2/3 RU","batch 4000, 1/3 RU","batch 500, 1/3 RU"]

	dfRU=[]
	dfNaive=[]
	dfInterest=[]
	for nam in range(len(names)):
		dfRU.append([])
		for i in range(nReps):
			try:
				file=open(fileName+names[nam]+"_run"+str(i+1)+"_coverageRU.csv")
				line=file.readline()
				line=line.replace(",\n","")
				arr=np.asarray(line.split(","),dtype=float)
				file.close()
				sal=int(len(arr)/shortening)
				arrShort=np.zeros(sal)
				for j in range(sal):
					arrShort[j]=np.mean(arr[j*shortening:(j+1)*shortening])
				dfRU[nam].append(arrShort)
				
			except FileNotFoundError:
				continue
		
		N=len(dfRU[nam][0])
		print(len(dfRU[nam]))
		print(N)
		dfNaive.append([])
		for i in range(nReps):
			try:
				file=open(fileName+names[nam]+"_run"+str(i+1)+"_coverageNaive.csv")
				line=file.readline()
				line=line.replace(",\n","")
				arr=np.asarray(line.split(","),dtype=float)
				file.close()
				sal=int(len(arr)/shortening)
				arrShort=np.zeros(sal)
				for j in range(sal):
					arrShort[j]=np.mean(arr[j*shortening:(j+1)*shortening])
				dfNaive[nam].append(arrShort)
			except FileNotFoundError:
				continue
		print(len(dfNaive[nam]))	
		
	
		n = len(dfNaive[nam])
		print(n)
		if n==0:
			print(fileName+names[nam])
			print("not found")
			exit()
			
		foundInterest=False
		if args.MLST:
			i=1
			while (not foundInterest) and i<=nReps:
				try:
					file=open(fileName+names[nam]+"_run"+str(i)+"_interestingRegions.csv")
					line=file.readline()
					line=line.replace(",\n","")
					dfInterest.append(np.asarray(line.split(","),dtype=int))
					file.close()
					foundInterest=True
					print(len(dfInterest[nam]))
				except FileNotFoundError:
					i+=1



elif (not args.coverage) and args.compareStrat:

	#df0 = pd.read_csv(fileName+'_run1_info.csv')
	#N=df0['N'][0]
	#totalbenefit = df0['totalbenefit'][0]
	#frameNum = df0['frames'][0]
	width = 1

	dfdistr = []
	descriptions=["batch 4000, full RU","batch 500, full RU","batch 4000, half RU","batch 500, half RU","batch 4000, 1/4 RU","batch500, 1/4 RU"]
	
	names=["_batchSize4000_timeFor1stUpdate1.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate1.0_strategyEvery1000000","_batchSize4000_timeFor1stUpdate100000000.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate100000000.0_strategyEvery1000000","_batchSize4000_timeFor1stUpdate150000000.0_strategyEvery4000000","_batchSize500_timeFor1stUpdate150000000.0_strategyEvery1000000"]
	if args.yeast:
		names=["_batchSize4000_timeFor1stUpdate1.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate1.0_strategyEvery3000000","_batchSize4000_timeFor1stUpdate200000000.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate200000000.0_strategyEvery3000000","_batchSize4000_timeFor1stUpdate400000000.0_strategyEvery12000000","_batchSize500_timeFor1stUpdate400000000.0_strategyEvery3000000"]
		descriptions=["batch 4000, full RU","batch 500, full RU","batch 4000, 2/3 RU","batch 500, 2/3 RU","batch 4000, 1/3 RU","batch 500, 1/3 RU"]
	timeRU = []
	timeNaive = []
	yRU=[]
	yNaive=[]
	trueRU=[]
	trueNaive=[]
	errProbRU=[]
	errProbNaive=[]
	benefitsnaive = []
	benefitsRU = []
	aveCoverageNaive = []
	aveCoverageRU=[]
	minCoverageNaive=[]
	minCoverageRU=[]
	#stratSizeNaive=[]
	#stratSizeRU=[]
	accptingProbNaive=[]
	accptingProbRU=[]
	computationalTimeRU=[]
	for j in range(len(names)):
		dfdistr.append([])
		timeRU.append([])
		timeNaive.append([])
		yRU.append([])
		yNaive.append([])
		trueRU.append([])
		trueNaive.append([])
		errProbRU.append([])
		errProbNaive.append([])
		benefitsnaive.append([])
		benefitsRU.append([])
		aveCoverageNaive.append([])
		aveCoverageRU.append([])
		minCoverageNaive.append([])
		minCoverageRU.append([])
		#stratSizeNaive.append([])
		#stratSizeRU.append([])
		accptingProbNaive.append([])
		accptingProbRU.append([])
		computationalTimeRU.append([])
		for i in range(nReps):
			try:
				dfdistr[j].append(pd.read_csv(fileName+names[j]+"_run"+str(i+1)+'_distr.csv'))		
			except FileNotFoundError:
				continue
	
		n = len(dfdistr[j])
		print(n)
		if n==0:
			print(fileName+names[j])
			print("not found")
			exit()
		
		N=dfdistr[j][0]["N"][0]

		for i in range(n):
			timeRU[j].append(dfdistr[j][i]["TimeRU"].tolist())
			timeNaive[j].append(dfdistr[j][i]["Time"].tolist())
		
		for i in range(n):
			trueRU[j].append(dfdistr[j][i]["ProbTruthRU"].tolist())
			trueNaive[j].append(dfdistr[j][i]["ProbTruth"].tolist())
			errProbRU[j].append(dfdistr[j][i]["errProbRU"].tolist())
			errProbNaive[j].append(dfdistr[j][i]["errProb"].tolist())
			yRU[j].append(dfdistr[j][i]["errProbRU"].tolist())
			yRU[j][i]=-10*np.log10(yRU[j][i]/N)
			yNaive[j].append(dfdistr[j][i]["errProb"].tolist())
			yNaive[j][i]=-10*np.log10(yNaive[j][i]/N)
			benefitsRU[j].append(dfdistr[j][i]["benefitsRU"].tolist())
			benefitsnaive[j].append(dfdistr[j][i]["benefits"].tolist())
			aveCoverageRU[j].append((dfdistr[j][i]["aveCoverageRU"]*normalization).tolist())
			aveCoverageNaive[j].append((dfdistr[j][i]["aveCoverage"]*normalization).tolist())
			minCoverageRU[j].append(dfdistr[j][i]["minCoverageRU"].tolist())
			minCoverageNaive[j].append(dfdistr[j][i]["minCoverage"].tolist())
			#stratSizeRU[j].append(dfdistr[j][i]["strategySizeRU"].tolist())
			#stratSizeNaive[j].append(dfdistr[j][i]["strategySize"].tolist())
			accptingProbNaive[j].append(dfdistr[j][i]["accptingProbNaive"].tolist())
			accptingProbRU[j].append(dfdistr[j][i]["accptingProbRU"].tolist())
			computationalTimeRU[j].append(dfdistr[j][i]["computationalTimeRU"].tolist())
	  
		finalTime = min(timeRU[0][i][-1] for i in range(n))
		x = np.linspace(0,finalTime,1000)
		for i in range(n):
			trueRU[j][i]=np.interp(x ,timeRU[j][i], trueRU[j][i])
			trueNaive[j][i]=np.interp(x ,timeNaive[j][i], trueNaive[j][i])
			errProbRU[j][i]=np.interp(x ,timeRU[j][i], errProbRU[j][i])
			errProbNaive[j][i]=np.interp(x ,timeNaive[j][i], errProbNaive[j][i])
			yRU[j][i]=np.interp(x ,timeRU[j][i], yRU[j][i])
			yNaive[j][i]=np.interp(x ,timeNaive[j][i], yNaive[j][i])
			benefitsRU[j][i]=np.interp(x ,timeRU[j][i], benefitsRU[j][i])
			benefitsnaive[j][i]=np.interp(x ,timeNaive[j][i], benefitsnaive[j][i])
			aveCoverageRU[j][i]=np.interp(x ,timeRU[j][i], aveCoverageRU[j][i])
			aveCoverageNaive[j][i]=np.interp(x ,timeNaive[j][i], aveCoverageNaive[j][i])
			minCoverageRU[j][i]=np.interp(x ,timeRU[j][i], minCoverageRU[j][i])
			minCoverageNaive[j][i]=np.interp(x ,timeNaive[j][i], minCoverageNaive[j][i])
			#stratSizeRU[j][i]=np.interp(x ,timeRU[j][i], stratSizeRU[j][i])
			#stratSizeNaive[j][i]=np.interp(x ,timeNaive[j][i], stratSizeNaive[j][i])
			accptingProbNaive[j][i]=np.interp(x ,timeNaive[j][i], accptingProbNaive[j][i])
			accptingProbRU[j][i]=np.interp(x ,timeRU[j][i], accptingProbRU[j][i])
			computationalTimeRU[j][i]=np.interp(x ,timeRU[j][i], computationalTimeRU[j][i])

else:

	df0 = pd.read_csv(fileName+'_run1_info.csv')
	N=df0['N'][0]
	totalbenefit = df0['totalbenefit'][0]
	frameNum = df0['frames'][0]
	width = 1

	dfdistr = []
	for i in range(n):
		try:
			dfdistr.append(pd.read_csv(fileName+"_run"+str(i)+'_distr.csv'))		
		except FileNotFoundError:
			continue
	
	n = len(dfdistr)
	print(n)
	timeRU = []
	timeNaive = []
	yRU=[]
	yNaive=[]
	trueRU=[]
	trueNaive=[]
	errProbRU=[]
	errProbNaive=[]
	benefitsnaive = []
	benefitsRU = []
	aveCoverageNaive = []
	aveCoverageRU=[]
	minCoverageNaive=[]
	minCoverageRU=[]
	stratSizeNaive=[]
	stratSizeRU=[]

	for i in range(n):
	  timeRU.append(dfdistr[i]["TimeRU"].tolist())
	  timeNaive.append(dfdistr[i]["Time"].tolist())
	finalTime = min(timeRU[i][-1] for i in range(n))
	x = np.linspace(0,finalTime,1000)
	for i in range(n):
	  trueRU.append(dfdistr[i]["ProbTruthRU"].tolist())
	  trueRU[i]=np.interp(x ,timeRU[i], trueRU[i])
	  trueNaive.append(dfdistr[i]["ProbTruth"].tolist())
	  trueNaive[i]=np.interp(x ,timeNaive[i], trueNaive[i])
	  errProbRU.append(dfdistr[i]["errProbRU"].tolist())
	  errProbRU[i]=np.interp(x ,timeRU[i], errProbRU[i])
	  errProbNaive.append(dfdistr[i]["errProb"].tolist())
	  errProbNaive[i]=np.interp(x ,timeNaive[i], errProbNaive[i])
	  yRU.append(dfdistr[i]["errProbRU"].tolist())
	  yRU[i]=-10*np.log10(yRU[i]/N)
	  yRU[i]=np.interp(x ,timeRU[i], yRU[i])
	  yNaive.append(dfdistr[i]["errProb"].tolist())
	  yNaive[i]=-10*np.log10(yNaive[i]/N)
	  yNaive[i]=np.interp(x ,timeNaive[i], yNaive[i])
	  benefitsRU.append(dfdistr[i]["benefitsRU"].tolist())
	  benefitsRU[i]=np.interp(x ,timeRU[i], benefitsRU[i])
	  benefitsnaive.append(dfdistr[i]["benefits"].tolist())
	  benefitsnaive[i]=np.interp(x ,timeNaive[i], benefitsnaive[i])
	  aveCoverageRU.append((dfdistr[i]["aveCoverageRU"]*normalization).tolist())
	  aveCoverageRU[i]=np.interp(x ,timeRU[i], aveCoverageRU[i])
	  aveCoverageNaive.append((dfdistr[i]["aveCoverage"]*normalization).tolist())
	  aveCoverageNaive[i]=np.interp(x ,timeNaive[i], aveCoverageNaive[i])
	  minCoverageRU.append(dfdistr[i]["minCoverageRU"].tolist())
	  minCoverageRU[i]=np.interp(x ,timeRU[i], minCoverageRU[i])
	  minCoverageNaive.append(dfdistr[i]["minCoverage"].tolist())
	  minCoverageNaive[i]=np.interp(x ,timeNaive[i], minCoverageNaive[i])
	  stratSizeRU.append(dfdistr[i]["strategySizeRU"].tolist())
	  stratSizeRU[i]=np.interp(x ,timeRU[i], stratSizeRU[i])
	  stratSizeNaive.append(dfdistr[i]["strategySize"].tolist())
	  stratSizeNaive[i]=np.interp(x ,timeNaive[i], stratSizeNaive[i])






def makePlot(timeRU,timeNaive,yRU,yNaive,fileNameEnding,title,yAxisTitle,logYaxis=False):
	meanRU = np.median(yRU,axis=0)
	highlinRU = np.percentile(yRU,90,axis=0)
	lowlinRU = np.percentile(yRU,10,axis=0)
	meannaive = np.median(yNaive,axis=0)
	highlinnaive = np.percentile(yNaive,90,axis=0)
	lowlinnaive = np.percentile(yNaive,10,axis=0)

	fig = go.Figure(layout=go.Layout(title=title, xaxis={'title':'Time','showgrid':True}, yaxis={'title':yAxisTitle,'showgrid':True}))

	t1 = go.Scatter(
		x = x,
		y = meanRU,
		mode='lines',
		name='Read Until strategy',
		marker=dict(color='blue')
	)
	t2 = go.Scatter(
		x = x,
		y = lowlinRU,
		mode='lines',
		marker=dict(color='blue'),
		showlegend=False
	)
	t3 = go.Scatter(
		x = x,
		y = highlinRU,
		line = dict(color = 'blue'),
		fill='tonexty',
		showlegend=False
	)

	t4 = go.Scatter(
		x = x,
		y = meannaive,
		mode='lines',
		name='Naive strategy',
		marker=dict(color='red'),
		line = dict(color = 'red'),
	)
	t5 = go.Scatter(
		x = x,
		y = lowlinnaive,
		mode='lines',
		marker=dict(color='red'),
		showlegend=False
	)
	t6 = go.Scatter(
		x = x,
		y = highlinnaive,
		mode='lines',
		marker=dict(color='red'),
		fill='tonexty',
		showlegend=False
	)

	fig.add_trace(t1)
	fig.add_trace(t2)
	fig.add_trace(t3)
	fig.add_trace(t4)
	fig.add_trace(t5)
	fig.add_trace(t6)
	
	if logYaxis:
		fig.update_layout(yaxis_type="log")

	if args.html:
		py.offline.plot(fig, filename=fileName+'_'+str(n)+'runs_'+fileNameEnding+'.html')
	fig.write_image(fileName+"_"+str(n)+'runs_'+fileNameEnding+'.pdf')













def makePlot_compareStrat(x,yRU,yRU500,yRUhalf,yRUhalf500,yRUquarter,yRUquarter500,yNaive,yNaive500,fileNameEnding,title,yAxisTitle,logYaxis=False,plotNaive=True):
	meanRU = np.median(yRU,axis=0)
	highlinRU = np.percentile(yRU,90,axis=0)
	lowlinRU = np.percentile(yRU,10,axis=0)
	meanRU2 = np.median(yRU500,axis=0)
	highlinRU2 = np.percentile(yRU500,90,axis=0)
	lowlinRU2 = np.percentile(yRU500,10,axis=0)
	meanRU3 = np.median(yRUhalf,axis=0)
	highlinRU3 = np.percentile(yRUhalf,90,axis=0)
	lowlinRU3 = np.percentile(yRUhalf,10,axis=0)
	meanRU4 = np.median(yRUhalf500,axis=0)
	highlinRU4 = np.percentile(yRUhalf500,90,axis=0)
	lowlinRU4 = np.percentile(yRUhalf500,10,axis=0)
	meanRU5 = np.median(yRUquarter,axis=0)
	highlinRU5 = np.percentile(yRUquarter,90,axis=0)
	lowlinRU5 = np.percentile(yRUquarter,10,axis=0)
	meanRU6 = np.median(yRUquarter500,axis=0)
	highlinRU6 = np.percentile(yRUquarter500,90,axis=0)
	lowlinRU6 = np.percentile(yRUquarter500,10,axis=0)
	meannaive = np.median(yNaive,axis=0)
	highlinnaive = np.percentile(yNaive,90,axis=0)
	lowlinnaive = np.percentile(yNaive,10,axis=0)
	meannaive2 = np.median(yNaive500,axis=0)
	highlinnaive2 = np.percentile(yNaive500,90,axis=0)
	lowlinnaive2 = np.percentile(yNaive500,10,axis=0)

	fig = go.Figure(layout=go.Layout(title=title, xaxis={'title':'Time','showgrid':True}, yaxis={'title':yAxisTitle,'showgrid':True}))

	t1 = go.Scatter(
		x = x,
		y = meanRU,
		mode='lines',
		name=descriptions[0],
		marker=dict(color='blue')
	)
	t2 = go.Scatter(
		x = x,
		y = lowlinRU,
		mode='lines',
		marker=dict(color='blue'),
		showlegend=False
	)
	t3 = go.Scatter(
		x = x,
		y = highlinRU,
		line = dict(color = 'blue'),
		fill='tonexty',
		showlegend=False
	)
	
	t12 = go.Scatter(
		x = x,
		y = meanRU2,
		mode='lines',
		name=descriptions[1],
		marker=dict(color='lightblue')
	)
	t22 = go.Scatter(
		x = x,
		y = lowlinRU2,
		mode='lines',
		marker=dict(color='lightblue'),
		showlegend=False
	)
	t32 = go.Scatter(
		x = x,
		y = highlinRU2,
		line = dict(color = 'lightblue'),
		fill='tonexty',
		showlegend=False
	)
	
	t13 = go.Scatter(
		x = x,
		y = meanRU3,
		mode='lines',
		name=descriptions[2],
		marker=dict(color='green')
	)
	t23 = go.Scatter(
		x = x,
		y = lowlinRU3,
		mode='lines',
		marker=dict(color='green'),
		showlegend=False
	)
	t33 = go.Scatter(
		x = x,
		y = highlinRU3,
		line = dict(color = 'green'),
		fill='tonexty',
		showlegend=False
	)
	
	t14 = go.Scatter(
		x = x,
		y = meanRU4,
		mode='lines',
		name=descriptions[3],
		marker=dict(color='lightgreen')
	)
	t24 = go.Scatter(
		x = x,
		y = lowlinRU4,
		mode='lines',
		marker=dict(color='lightgreen'),
		showlegend=False
	)
	t34 = go.Scatter(
		x = x,
		y = highlinRU4,
		line = dict(color = 'lightgreen'),
		fill='tonexty',
		showlegend=False
	)
	
	t15 = go.Scatter(
		x = x,
		y = meanRU5,
		mode='lines',
		name=descriptions[4],
		marker=dict(color='purple')
	)
	t25 = go.Scatter(
		x = x,
		y = lowlinRU5,
		mode='lines',
		marker=dict(color='purple'),
		showlegend=False
	)
	t35 = go.Scatter(
		x = x,
		y = highlinRU5,
		line = dict(color = 'purple'),
		fill='tonexty',
		showlegend=False
	)
	t16 = go.Scatter(
		x = x,
		y = meanRU6,
		mode='lines',
		name=descriptions[5],
		marker=dict(color='violet')
	)
	t26 = go.Scatter(
		x = x,
		y = lowlinRU6,
		mode='lines',
		marker=dict(color='violet'),
		showlegend=False
	)
	t36 = go.Scatter(
		x = x,
		y = highlinRU6,
		line = dict(color = 'violet'),
		fill='tonexty',
		showlegend=False
	)
	
	if plotNaive:

		t4 = go.Scatter(
			x = x,
			y = meannaive,
			mode='lines',
			name='Naive strategy 4000 read batch',
			marker=dict(color='red'),
			line = dict(color = 'red'),
		)
		t5 = go.Scatter(
			x = x,
			y = lowlinnaive,
			mode='lines',
			marker=dict(color='red'),
			showlegend=False
		)
		t6 = go.Scatter(
			x = x,
			y = highlinnaive,
			mode='lines',
			marker=dict(color='red'),
			fill='tonexty',
			showlegend=False
		)
		t42 = go.Scatter(
			x = x,
			y = meannaive2,
			mode='lines',
			name='Naive strategy 500 read batch',
			marker=dict(color='orange'),
			line = dict(color = 'orange'),
		)
		t52 = go.Scatter(
			x = x,
			y = lowlinnaive2,
			mode='lines',
			marker=dict(color='orange'),
			showlegend=False
		)
		t62 = go.Scatter(
			x = x,
			y = highlinnaive2,
			mode='lines',
			marker=dict(color='orange'),
			fill='tonexty',
			showlegend=False
		)
		fig.add_trace(t4)
		fig.add_trace(t5)
		fig.add_trace(t6)
		fig.add_trace(t42)
		fig.add_trace(t52)
		fig.add_trace(t62)

	fig.add_trace(t1)
	fig.add_trace(t2)
	fig.add_trace(t3)
	fig.add_trace(t12)
	fig.add_trace(t22)
	fig.add_trace(t32)
	fig.add_trace(t13)
	fig.add_trace(t23)
	fig.add_trace(t33)
	fig.add_trace(t14)
	fig.add_trace(t24)
	fig.add_trace(t34)
	fig.add_trace(t15)
	fig.add_trace(t25)
	fig.add_trace(t35)
	fig.add_trace(t16)
	fig.add_trace(t26)
	fig.add_trace(t36)
	
	if logYaxis:
		fig.update_layout(yaxis_type="log")

	if args.html:
		py.offline.plot(fig, filename=fileName+'_'+fileNameEnding+'.html')
	fig.write_image(fileName+"_"+fileNameEnding+'.pdf')
	
	
	
	











def makePlotCoverage_compareStrat(countRU,count,title="Coverage at the end of the sequencing run",interest=None,fileNameEnding="Coverage",logYaxis=False):
	
	print("Making plot of coverage")
	medianRU=[]
	highlinRU=[]
	lowlinRU=[]
	for nam in range(len(countRU)):
		medianRU.append(np.median(countRU[nam],axis=0))
		highlinRU.append( np.percentile(countRU[nam],90,axis=0))
		lowlinRU.append( np.percentile(countRU[nam],10,axis=0))
	mediannaive=[]
	highlinnaive=[]
	lowlinnaive=[]
	for nam in range(len(count)):
		mediannaive.append(np.median(count[nam],axis=0))
		highlinnaive.append( np.percentile(count[nam],90,axis=0))
		lowlinnaive.append( np.percentile(count[nam],10,axis=0))
	
	colors=['blue','lightblue','green','lightgreen','purple','violet']
	colors2=['red','orange']
	descriptionsNaive=['Naive strategy 4000 read batch','Naive strategy 500 read batch']
	descriptionsRU=['Read Until 4000 read batch','Read Until 500 read batch','Half Read Until 4000 reads batch','Half Read Until 500 reads batch','Quarter Read Until 4000 reads batch','Quarter Read Until 500 reads batch']
	fig = go.Figure(layout=go.Layout(title=title, xaxis={'title':'Chromosome position','showgrid':True}, yaxis={'title':'Coverage','showgrid':True}))
	for nam in range(len(countRU)):
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = medianRU[nam],mode='lines',name=descriptions[nam],marker=dict(color=colors[nam]),line = dict(width = 0.2, color = colors[nam])))
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = highlinRU[nam],mode='lines',marker=dict(color=colors[nam]),line = dict(width = 0.2, color = colors[nam]),showlegend=False))
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = lowlinRU[nam],mode='lines',marker=dict(color=colors[nam]),line = dict(width = 0.2, color = colors[nam]),fill='tonexty',showlegend=False))
	for nam in range(2):
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = mediannaive[nam],mode='lines',name=descriptionsNaive[nam],marker=dict(color=colors2[nam]),line = dict(width = 0.2, color = colors2[nam])))
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = highlinnaive[nam],mode='lines',marker=dict(color=colors2[nam]),line = dict(width = 0.2, color = colors2[nam]),showlegend=False))
		fig.add_trace(go.Scatter(x = np.arange(N)*shortening*1000,y = lowlinnaive[nam],mode='lines',marker=dict(color=colors2[nam]),line = dict(width = 0.2, color = colors2[nam]),fill='tonexty',showlegend=False))
	
	if args.MLST and foundInterest:
		i=0
		Found=False
		#print("plotting interesting regions")
		while i<N*shortening:
			if interest[i]==0:
				i+=1
			else:
				#print(i)
				start=i
				while i<N*shortening and interest[i]>0:
					i+=1
				end=i
				#print([int(start),int((end-1))])
				if (end-start<shortening):
					end=start+shortening+1
				#fig.axhline(y=0, xmin=start, xmax=end-1, color='green', linestyle='-', lw=2)
				t3 = go.Scatter(
				x = [int(start*1000),int((end*1000-1))],
				y = [0,0],
				mode='lines',
				name='Regions of interest',
				marker=dict(color='green'),
				line = dict(color = 'green'),
				showlegend=(not Found)
				)
				Found=True
				fig.add_trace(t3)
	
	if logYaxis:
		fig.update_layout(yaxis_type="log")

	if args.html:
		py.offline.plot(fig, filename=fileName+'_'+fileNameEnding+'.html')
	fig.write_image(fileName+'_'+fileNameEnding+'.pdf')














	
	






def makePlotCoverage(countRU,count,title="Coverage at the end of the sequencing run",interest=None,fileNameEnding="Coverage",logYaxis=False):
	
	print("Making plot of coverage")
	medianRU = np.median(countRU,axis=0)
	highlinRU = np.percentile(countRU,90,axis=0)
	lowlinRU = np.percentile(countRU,10,axis=0)
	mediannaive = np.median(count,axis=0)
	highlinnaive = np.percentile(count,90,axis=0)
	lowlinnaive = np.percentile(count,10,axis=0)
	
	fig = go.Figure(layout=go.Layout(title=title, xaxis={'title':'Chromosome position','showgrid':True}, yaxis={'title':'Coverage','showgrid':True}))
	
	
	t1 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = medianRU,
		mode='lines',
		name='Read Until strategy',
		marker=dict(color='blue'),
		line = dict(width = 0.2, color = 'blue')
	)
	fig.add_trace(t1)
	
	t3 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = highlinRU,
		mode='lines',
		line=dict(width = 0.2, color='blue'),
		marker=dict(color='blue'),
		showlegend=False
	)
	fig.add_trace(t3)
	
	t4 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = lowlinRU,
		mode='lines',
		line=dict(width = 0.2, color='blue'),
		fill='tonexty',
		showlegend=False
	)
	fig.add_trace(t4)

	t2 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = mediannaive,
		mode='lines',
		name='Naive strategy',
		marker=dict(color='red'),
		#line = dict(color = 'red')
		line = dict(width = 0.2, color = 'red')
	)
	fig.add_trace(t2)
	t5 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = highlinnaive,
		mode='lines',
		line = dict(width = 0.2,color = 'red'),
		marker=dict(color='red'),
		showlegend=False
	)
	fig.add_trace(t5)
	t6 = go.Scatter(
		x = np.arange(N)*shortening*1000,
		y = lowlinnaive,
		mode='lines',
		line = dict(width = 0.2,color = 'red'),
		fill='tonexty',
		showlegend=False
	)
	fig.add_trace(t6)
	
	if args.MLST and foundInterest:
		i=0
		Found=False
		#print("plotting interesting regions")
		while i<N*shortening:
			if interest[i]==0:
				i+=1
			else:
				#print(i)
				start=i
				while i<N*shortening and interest[i]>0:
					i+=1
				end=i
				#print([int(start),int((end-1))])
				if (end-start<shortening):
					end=start+shortening+1
				#fig.axhline(y=0, xmin=start, xmax=end-1, color='green', linestyle='-', lw=2)
				t3 = go.Scatter(
				x = [int(start*1000),int((end*1000-1))],
				y = [0,0],
				mode='lines',
				name='Regions of interest',
				marker=dict(color='green'),
				line = dict(color = 'green'),
				showlegend=(not Found)
				)
				Found=True
				fig.add_trace(t3)
	
	if logYaxis:
		fig.update_layout(yaxis_type="log")

	if args.html:
		py.offline.plot(fig, filename=fileName+'_'+str(len(countRU))+'runs_'+fileNameEnding+'.html')
	fig.write_image(fileName+'_'+str(len(countRU))+'runs_'+fileNameEnding+'.pdf')





if args.coverage:
	if args.MLST:
		if args.compareStrat:
			makePlotCoverage_compareStrat(dfRU,dfNaive,title="Coverage at the end of the sequencing run",interest=dfInterest[0],fileNameEnding="Coverage")
		else:
			makePlotCoverage(dfRU,dfNaive,title="Coverage at the end of the sequencing run",interest=dfInterest,fileNameEnding="Coverage")
	else:
		if args.compareStrat:
			makePlotCoverage_compareStrat(dfRU,dfNaive,title="Coverage at the end of the sequencing run",fileNameEnding="Coverage")
		else:
			makePlotCoverage(dfRU,dfNaive,title="Coverage at the end of the sequencing run",fileNameEnding="Coverage")

elif (not args.coverage) and args.compareStrat:
	makePlot_compareStrat(x,yRU[0],yRU[1],yRU[2],yRU[3],yRU[4],yRU[5],yNaive[0],yNaive[1],"qualityScores","Reconstruction error as quality score per position",'-10*(log10(average error))',logYaxis=False)
	makePlot_compareStrat(x,trueRU[0],trueRU[1],trueRU[2],trueRU[3],trueRU[4],trueRU[5],trueNaive[0],trueNaive[1],"callingPrecision","Precision of genome reconstruction",'Average probability of true base',logYaxis=False)
	makePlot_compareStrat(x,errProbRU[0],errProbRU[1],errProbRU[2],errProbRU[3],errProbRU[4],errProbRU[5],errProbNaive[0],errProbNaive[1],"callingErrors","Expected errors of genome reconstruction",'Expected errors',logYaxis=True)
	makePlot_compareStrat(x,benefitsRU[0],benefitsRU[1],benefitsRU[2],benefitsRU[3],benefitsRU[4],benefitsRU[5],benefitsnaive[0],benefitsnaive[1],"benefits","Remaining positional score",'Sum of position scores',logYaxis=True)
	makePlot_compareStrat(x,aveCoverageRU[0],aveCoverageRU[1],aveCoverageRU[2],aveCoverageRU[3],aveCoverageRU[4],aveCoverageRU[5],aveCoverageNaive[0],aveCoverageNaive[1],"aveCoverage","Average coverage achieved",'Average coverage',logYaxis=False)
	makePlot_compareStrat(x,minCoverageRU[0],minCoverageRU[1],minCoverageRU[2],minCoverageRU[3],minCoverageRU[4],minCoverageRU[5],minCoverageNaive[0],minCoverageNaive[1],"minCoverage","Minimum coverage achieved",'Minimum coverage',logYaxis=False)
	makePlot_compareStrat(x,accptingProbRU[0],accptingProbRU[1],accptingProbRU[2],accptingProbRU[3],accptingProbRU[4],accptingProbRU[5],accptingProbNaive[0],accptingProbNaive[1],"acceptedFrags","proportion of fragments not rejected by a strategy",'Proportion accepted fragments',logYaxis=False)
	makePlot_compareStrat(x,computationalTimeRU[0],computationalTimeRU[1],computationalTimeRU[2],computationalTimeRU[3],computationalTimeRU[4],computationalTimeRU[5],computationalTimeRU[0],computationalTimeRU[1],"compTime","cumulative CPU time spent updating the strategy",'Computational strategy cost',logYaxis=False,plotNaive=False)
else:
	makePlot(timeRU,timeNaive,yRU,yNaive,"qualityScores","Reconstruction error as quality score per position",'-10*(log10(average error))',logYaxis=False)
	makePlot(timeRU,timeNaive,trueRU,trueNaive,"callingPrecision","Precision of genome reconstruction",'Average probability of true base',logYaxis=False)
	makePlot(timeRU,timeNaive,errProbRU,errProbNaive,"callingErrors","Expected errors of genome reconstruction",'Expected errors',logYaxis=True)
	makePlot(timeRU,timeNaive,benefitsRU,benefitsnaive,"benefits","Remaining positional score",'Sum of position scores',logYaxis=True)
	makePlot(timeRU,timeNaive,aveCoverageRU,aveCoverageNaive,"aveCoverage","Average coverage achieved",'Average coverage',logYaxis=False)
	makePlot(timeRU,timeNaive,minCoverageRU,minCoverageNaive,"minCoverage","Minimum coverage achieved",'Minimum coverage',logYaxis=False)
	makePlot(timeRU,timeNaive,stratSizeRU/stratSizeNaive[0],stratSizeNaive/stratSizeNaive[0],"stratSize","proportion of fragments not rejected",'Strategy size',logYaxis=False)

exit()

