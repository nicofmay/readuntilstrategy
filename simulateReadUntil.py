from __future__ import division 
import numpy as np
import pandas as pd
import copy
import time
import os
import argparse


parser = argparse.ArgumentParser(description='Find best masking strategy, and updating it after every batch of new reads.')
parser.add_argument("-N", nargs='+', help="Chromosome sizes N_i for any number of chromosomes i=1,....", type=int, default=4000000)
parser.add_argument("-mu", help="Length mu of fragment piece used for mapping fragments to the reference genome", type=int, default=500)
parser.add_argument("-rho", help="Time rho (in units of basepairs that could be read by a nanopore in the same time) that it takes to eject a fragment from a nanopore when a fragment is to be rejected (not read)", type=float, default=500)
parser.add_argument("-alpha", help="Time alpha (again in units of bp read) that it takes to find a new fragment for a nanopore after either a read is complete or a fragments has just been rejected", type=float, default=300)
parser.add_argument("-e", help="Probability e that a bp in a nanopore read represents the wrong nucleotide (substitution sequencing error probability)", type=float, default=0.06)
parser.add_argument("-theta", help="expected genetic diversity from the reference (in terms of 4*Ne*mu). This means the proportion of differences expected between the reference and the sequenced genome. If 0 then assumes reference is not known, but typical values would be 0.01 (default) for many species or 0.001 for humans.", type=float, default=0.01)
parser.add_argument("-r", help="natural ratio of deletion to substitution rate. Typical values would be around 0.4 (default).", type=float, default=0.4)
parser.add_argument("-errD", help="probability that a base in the genome is skipped in a read (appears deleted in the read while in truth it's not). A realistic value would be something like 0.03 (default).", type=float, default=0.03)
parser.add_argument("-errWeird", help="Probability that an actual deletion is missed in a read. This is the probability that, if the sequenced genome has a deletion, this deletion will not appear in the considered read position.", type=float, default=0.1)
parser.add_argument("-stoptime", help="Stop simulations after this time (approximately equal to final coverage times genome size)", type=float, default=200000000)
parser.add_argument("-lam", help="mean length lambda of the normal distribution of read length to be created. Distribution truncated to fall between maximum(mu , lambda-10*sd) and minimum(N/2,lambda+10*sd).", type=float, default=3000.0)
parser.add_argument("-sd", help="Standard deviation of normal distribution of read lengths to be created. If sd=0 then an approximate Poisson distribution is used (normal with sd=sqrt(lam))", type=float, default=6000.0)
parser.add_argument("-readLengthsFromFile", help="should I read the distribution of read lengths from a file? If yes, options -lambda and -sd are ignored, and read lengths will be input from file distrRU.csv", action="store_true")
#parser.add_argument("-byPercent", help="should I stop simulations once a certain perrcentage of the total benefit has been reached?", action="store_true")
#parser.add_argument("-stoppercent", help="Stop simulations of naive strategy when this proportion of total benefit has been reached (if using byPercent)", type=float, default=0.95)
#parser.add_argument("-timeToRecord", help="Time (again in units of bp read) until we record all the parameters of the simulations", type=int, default=400000)
#parser.add_argument("-timeToUpdate", help="time until we update the decision strategy", type=int, default=2000000)
parser.add_argument("-runID", help="Number of run", type=int, default=1)
#parser.add_argument("-savecounts", help="should I save the base counts for each genome position?", action="store_true")
parser.add_argument("-saveFinalCounts", help="should I save to file the final base counts for each genome position?", action="store_true")
#parser.add_argument("-savereads", help="should I save all simulated reads?", action="store_true")
parser.add_argument("-coverageVariability", help="should I simulated  variability of coverage along the genome? if yes, this is the ratio fo the highest coverage versus lowest coverage (default=1, no bias). Coverage variability is simulated as a sine function of nPeaks peaks", type=float, default=1.0)
parser.add_argument("-nPeaks", help="Number of peaks in the sine function simulating coverage variability along the genome", type=float, default=20.0)
parser.add_argument("-nLoci", help="Number of loci that are considered intersting in the genome. Defaul (0) considers the whole genome as interesting. 10 loci each 500bp might reflect a classical MLST, while 100 loci each 10kb might be more similar to a core genome MLST.", type=int, default=0)
parser.add_argument("-lenLoci", help="Length of each interesting locus", type=int, default=500)
parser.add_argument("-linear", help="is the chromosome linear, and therefore should I correct for end-of-chromosome effects?", action="store_true")
parser.add_argument("-diploid", help="Is the sequenced genome diploid? By default assumes haploid (other scenarios not implemented yet)", action="store_true")
parser.add_argument("-batchSize", help="Number of reads in a single batch (default 4000). If =1 means that batch behaviour is ignored.", type=int, default=4000)
#parser.add_argument("-firstBatchSize", help="Number of reads in the first batch. If =1 means that batch behaviour is ignored.", type=int, default=1)
parser.add_argument("-timeFor1stUpdate", help="Time before the first strategy update is done. default=0 means that the first update is done as soon as the first batch arrives", type=float, default=0.0)
parser.add_argument("-strategyEvery", help="Time to wait before a new strategy update. default=1 means that the strategy is updated every batch of reads.", type=int, default=1)
parser.add_argument("-updateOnlyWhenNecessary", help="should I update posterior probabilities etc only when updating the strategy (reduces running time but makes plots coarser)?", action="store_true")
parser.add_argument("-approximate", help="should I use the approximate calculation of positional scores?", action="store_true")
parser.add_argument("-thresholdApproximation", help="Threshold for neglecting positions with small scores (default=10000)", type=float, default=10000.0)


args = parser.parse_args()
#N=args.N
chromLens=args.N
N=int(sum(chromLens))
mu=args.mu
rho = args.rho
alpha = args.alpha 
e= args.e
theta=args.theta
delRatio=args.r
errD=args.errD
errWeird=args.errWeird
stoptime=args.stoptime
lam=args.lam
sd=args.sd
#byPercent=args.byPercent #needs to be false if creating graphs from multiple runs
#stoppercent = args.stoppercent 
#countpartsize = args.timeToRecord
#readspert = args.timeToUpdate
covRatio= args.coverageVariability
nPeaks=args.nPeaks
nLoci=args.nLoci
lenLoci=args.lenLoci
linear=args.linear
diploid=args.diploid
batchSize=args.batchSize
#firstBatchSize=args.firstBatchSize
updateOnlyWhenNecessary=args.updateOnlyWhenNecessary
approximate=args.approximate
thresholdApproximation=args.thresholdApproximation

#Record time spent for different tasks
testing=True
if testing:
	timeUpdateS=0.0
	timeUpdateSnaive=0.0
	timeUpdateSmu=0.0
	timeUpdateCount=0.0
	timeUpdateCountNaive=0.0
	timeUpdateStrategy=0.0
	timeGenerateReads=0.0
	timeStoring=0.0
	timeLogging=0.0
	timeTotal=0.0
	startTotal = time.time()


print("Initialisation")

#Should I simulate reverse reads?
reversereads = True

#If there are no deletions considered, both in the sequenced genome and in sequencing errors, then 4 bases need to be considered
if (delRatio<0.000000000001 or theta<0.000000000001) and errD<0.000000000001:
	if not diploid:
		Bases = ['A','C','G','T']
		Genotypes = ['A','C','G','T']
		#basedict = {"A": 0,"C": 1,"G": 2, "T": 3}
	else:
		Bases = ['A','C','G','T']
		Genotypes = ['AA','AC','AG','AT','CC','CG','CT','GG','GT','TT']
		#basedict = {"AA": 0,"AC": 1,"AG": 2, "AT": 3,"CC": 4,"CG": 5,"CT": 6, "GG": 7,"GT": 8,"TT": 9}
	lenB=len(Bases)
	lenG=len(Genotypes)
	#phi is as \phi defined in manuscript sections 0.1.1 or 0.1.2 depending on ploidy.
	#Here, there is no indels.
	#phi[b][g] is prob we read b given the genotype is g
	phi = np.zeros((lenB,lenG)) 
	if not diploid:
		for b in range(lenB):
			for g in range(lenG):
				if b==g:
					phi[b][g] = 1-e
				else:
					phi[b][g] = e/(lenB-1)
	else: #phi for diploid genome without deletions
		for b in range(lenB):
			for g in range(lenG):
				countBase=Genotypes[g].count(Bases[b])
				if countBase==2:
					phi[b][g] = 1-e
				elif countBase==1:
					phi[b][g] = (1-e)/2 + e/(2*(lenB-1))
				elif countBase==0:
					phi[b][g] = e/(lenB-1)

#If needed, consider also the gap base "-"
else:
	if not diploid:
		Bases = ['A','C','G','T','-']
		Genotypes = ['A','C','G','T','-']
	else:
		Bases = ['A','C','G','T','-']
		Genotypes = ['AA','AC','AG','AT','CC','CG','CT','GG','GT','TT','A-','C-','G-','T-','--']
	lenB=len(Bases)
	lenG=len(Genotypes)
	#basedict = { "A": 0, "C": 1,"G": 2,"T": 3, "-": 4}		
	noErrP=(1.0-(e+errD))
	#phi with deletions
	phi = np.zeros((lenB,lenG))
	if not diploid:
		for b in range(lenB-1):
			for g in range(lenG-1):
				if b==g:
					phi[b][g]=noErrP
				else:
					phi[b][g]=e/(lenB-2)
			#phi[b][lenB-1]=errD
			phi[b][lenG-1]=errWeird/(lenB-1)
		for g in range(lenG-1):
			#phi[lenB-1][g]=errWeird/(lenB-1)
			phi[lenB-1][g]=errD
		phi[lenB-1][lenG-1]=1.0-errWeird
	else: #phi for diploid genome with deletions
		for b in range(lenB-1):
			for g in range(lenG-5):
				countBase=Genotypes[g].count(Bases[b])
				if countBase==2:
					phi[b][g] = noErrP
				elif countBase==1:
					phi[b][g] = noErrP/2 + e/(2*(lenB-2))
				elif countBase==0:
					phi[b][g] = e/(lenB-2)
			for i in range(4):
				g=10+i
				countBase=Genotypes[g].count(Bases[b])
				if countBase==1:
					phi[b][g] = noErrP/2 + errWeird/(2*(lenB-1))
				elif countBase==0:
					phi[b][g] = e/(2*(lenB-2)) + errWeird/(2*(lenB-1))
			phi[b][lenG-1]=errWeird/(lenB-1)
		for g in range(lenG):
			countGap=Genotypes[g].count(Bases[lenB-1])
			if countGap==2:
				phi[lenB-1][g] = 1.0-errWeird
			elif countGap==1:
				phi[lenB-1][g] = (1.0-errWeird)/2 + errD/2
			elif countGap==0:
				phi[lenB-1][g] = errD
print("Bases:")
print(Bases)
print("Genotypes:")
print(Genotypes)
print("phi: ")
print(phi)

#calculate proportion of homozygote derived SNPs:
popsize=1000
homo=0.0
hetero=0.0
aN=np.sum(1.0/(np.arange(popsize+1)[1:]))
for i in range(popsize):
	homo+=(1.0/((i+1)*aN))*((i+1)*float(i+1)/(popsize**2))
	hetero+=(1.0/((i+1)*aN))*2*((popsize-(i+1))*float(i+1)/(popsize**2))
propHomo=homo/(homo+hetero)

#create a reference genome, which is assumed to be made only of A's (represented as 0's). Multiple chromsomes are represented as concatenated.
if theta<0.000000000001:
	genome=np.zeros(N, dtype=np.dtype('uint8'))
else: #if theta>0, then the sequenced genome can be different from the reference, because here we add genetic diversity to it
	if theta*(1.0+delRatio)>1.0: #too much diversity makes no sense
			print("PROBLEM: theta="+str(theta)+" (with indels "+str(theta*(1.0+delRatio))+") too high or indel rate too high, almost all genome expected to be different from the reference")
			exit()
	if not diploid:
		probs=np.zeros(lenB)
		probs[0]=1.0-(theta*(1+delRatio))
		for i in range(3):
			probs[i+1]=theta/3
		if delRatio>0.000000000001:
			probs[4]=theta*delRatio
		#each position of the sequenced genome can be randomly mutated wrt the reference
		genome=np.random.choice(lenB,size=N, p=probs)
	else:
		probs=np.zeros(lenG)
		probs[0]=1.0-(theta*(1+delRatio))
		for i in range(3):
			probs[i+1]=(1.0-propHomo)*theta/3
		probs[4]=(propHomo)*theta/3
		probs[7]=(propHomo)*theta/3
		probs[9]=(propHomo)*theta/3
		if delRatio>0.000000000001:
			probs[10]=(1.0-propHomo)*theta*delRatio
			probs[14]=(propHomo)*theta*delRatio
		#each position of the sequenced genome can be randomly mutated wrt the reference
		genome=np.random.choice(lenG,size=N, p=probs)
print("Genome to be sequenced: ")
print(genome)


#A big range array computed only once and used multiple times for loops (for computational convenience)
range2N=np.arange(2*N, dtype=np.dtype('uint32'))


#L is the distribution of probabilities for possible read lengths, and can either be read from file distrRU.csv, or can be calculated on the spot from input parameters lam (the mode of the distribution) and sd (it's dispersion).
#This is the same as function L(l) in the manuscript section 0.1.3
#Here one has to consider read length of a "natural" sequencing run, that is, without read until.
#This basically tells you how long and how variable in length one expects the reads to be.
#I have now changed the code so that L doesn't need to be as long as the chromosome.
#L is used here for simulating reads, for the strategy instead we use an approximation CLcomp defined below.
if args.readLengthsFromFile: #read length distribution from file
	df = pd.read_csv('distrRU.csv')
	lenL=len(df['p'])
	L = np.zeros(lenL)
	for i in range(lenL):
		L[i]=df['p'][i]
	#maximum possible read length.
	lastl=lenL
	while L[lastl-1]<0.000000000000001:
		lastl-=1
	L/=sum(L)
	# lam is \lambda, the average read length.
	#It's useful for finding the best strategy, in section 0.2.2 and in the code below
	lam = np.average(range(lenL),weights=L)+1
	print("Lamdba: "+str(lam))
else: #instead create read length distribution on the spot
	if lam<0.000000000001:
		print("ERROR: if read length distribution is not taken from file, lambda has to be positive so to generate read length distribution.")
		exit()
	if sd>0.000000000001:
		print("Positive sd, generating truncated normal distribution of read lengths")
	else:
		print("Non-positive sd, generating truncated approximate Poisson distribution of read lengths")
		sd=np.sqrt(lam)
	lenL=max(mu+1,min(N,int(lam+10*sd)))
	lastl=lenL
	print("Maximum read length: "+str(lenL))
	x = range2N[:lenL]
	L = np.exp(-(x-lam+1)**2/(2*(sd**2)))/(sd*np.sqrt(2*np.pi))
	#we don't allow reads shorter than mu. If those happen in real life, than a strategy is not needed for their outcome.
	L[:mu]=0.0
	L/=sum(L)
	lam = np.average(x,weights=L)+1
	print("New lambda after truncation:"+str(lam)) #Now lam means read length (like the \lambda in the mansucript) and not the distribution mode like args.lam
	data = list(zip(x+1,L))
	df = pd.DataFrame(data = data, columns = ['position','p'])
	df.to_csv('distrRU_lam'+str(int(args.lam))+'_sd'+str(int(args.sd))+'.csv',index=False)


#CLcomp is 1-CL, with CL the cumulative distribution of read lengths.
#This is the same as \tilde CL in the manuscript section 0.1.3
#The CLcomp starts at 1 and decreases as one increases the considered length.
#CLcomp[i] represents the probability that a random fragment (read) is at least i+1 long.
CLcomp = np.zeros(lenL)
CLcomp[0] = 1
for i in range(1,lenL):
  CLcomp[i] = CLcomp[i-1] - L[i-1]
  if CLcomp[i]<0.000000001: #if Clcomp[i] is very near zero for any i, this is a numerical error and should be zero, so here we correct it with threshold 0.00001
    CLcomp[i]=0
    
print("Approximation to CLcomp:")
#now CLcomp is replaced by a piece-wise constant function.
#This is explained in the manuscript section 0.1.4 part "1) piecewise constant function".
#partitionNum determines how many pieces to have; having 30 pieces makes updating the scores 3 times slower than having 10 pieces (at least I suspect) so careful not to use too high partitionNum,
#however higher partitionNum could improve the strategy.
#dummyPart[i] tells you that the probability of reads being long at least dummyPart[i] is approximated by probability 1-i/(partitionNum-1), 
#while the probability of reads being long at least dummyPart[i]+1 is approximated by probability 1-(i+1)/(partitionNum-1).
#This is the same as saying that dummyPart[i] is the point of the i-th change of value for the piece-wise constant function.
#dummyPart contains partitionNum-1 values because the first and the last approximating probabilities are always 1 and 0 respectively.
partitionNum = 11 #Only used for CLcomp. Used to approx U with a piecewise constant function with partitionNum partitions- the greater the more accurate but slower (see find_strat())
dummyPart=np.zeros(partitionNum-1,dtype=np.dtype('uint32'))
i=0
for part in range(partitionNum-1):
        while CLcomp[i]>1-(part+0.5)/(partitionNum-1):
                i+=1
        dummyPart[part] = i  
print(dummyPart)


# we want to create array of expected read length for positions close to the end of the genome in case of linear chromosome
# this is for having the correct time cost for accepting fragments close to the end of the chromosome.
if linear:
	lams=np.zeros(lastl)
	lams[lastl-1]=lam
	totSum=lam
	lastTerm=0.0
	for i in range2N[:lastl-1]:
		totSum-=L[lastl-(i+1)]*(lastl-i)
		lastTerm+=L[lastl-(i+1)]
		lams[lastl-(i+2)]=totSum+lastTerm*(lastl-(i+1))


#F[i] contains the probability that a new fragment (read) starts at position i+1 of the genome.
#For now, like in the main text, this is assumed to be uniform, so, in practice, one might as well just remove this completely without affecting the results.
#We are keeping this for now as one might want to later improve results by better modeling GC sequencing bias and chromosome end conditions by using F.
F = np.full(N,1/float(N))
# F2 is the same as F, but for reverse direction reads.
F2 = np.full(N,1/float(N))

#If we have to simulate variability in coverage, define the new Fsimu and F2simu for sampling the reads
covRatio= args.coverageVariability
nPeaks=args.nPeaks
x=range2N[:N]/N
coeffPi=2*np.pi*nPeaks
if covRatio>1.000000000001:
	Fsimu=0.5+covRatio/2+(covRatio/2-0.5)*np.sin(x*coeffPi)
	Fsimu/=np.sum(Fsimu)
	F2simu=0.5+covRatio/2+(covRatio/2-0.5)*np.sin((x-lam/N)*coeffPi)
	F2simu/=np.sum(F2simu)
else:
	Fsimu=F
	F2simu=F2

#I pre-calculate these for computational convenience
Fhalf=F*0.5
F2half=F2*0.5

#number of chromosomes etc
chNum=len(chromLens)
chArray=np.asarray(chromLens,dtype=np.dtype('uint32'))
chCumsum=np.append(np.zeros(1,dtype=np.dtype('uint32')),np.cumsum(chArray,dtype=np.dtype('uint32')))

# t[i] is t_i  defined in Supplementary Section 2.
#values of t up to index N-1 are for forward fragments, from N to 2N-1 are for backward fragments.
#t[i] is the expected cost increase by keeping reading a fragment mapping at i instead of rejecting it.
#t[i] can be negative, meaning that it's expected to be faster to read the fragment until the end than rejecting it.
#if t[i] is negative, fragments mapping at i should not be rejected.
t=np.append(Fhalf*(lam-mu-rho),F2half*(lam-mu-rho))
if linear:
	for c in range2N[:chNum]:
		for i in range2N[:lastl]:
			t[N+chCumsum[c]+i]=F2half[chCumsum[c]+i]*(lams[i]-mu-rho)
			t[chCumsum[c+1]-(i+1)]=Fhalf[chCumsum[c+1]-(i+1)]*(lams[i]-mu-rho)
    
#average time cost of initial strategy where all fragments are rejected.
#(like T^0 at the end of Supplementary Section 2).
T0 = alpha + mu + rho
if linear:
	for c in range2N[:chNum]:
		for i in range2N[:mu-1]:
			T0-=Fhalf[chCumsum[c+1]-(i+1)]*(mu-1-i)
			T0-=F2half[chCumsum[c]+i]*(mu-1-i)
			


#regions of interest
regions=[]

#List containing simulated read starting positions - it comes handy when simulating coverage variation and we don't want
#to simulate one read position at the time for computational efficiency
readPosSampled=[0,0]
#readPoss=[]
#reverse reads
#readPosSampled2=[0]
#readPoss2=[]
readPoss=[np.random.choice(N,size=100+int(stoptime/(4*lam)),p=Fsimu),np.random.choice(N,size=100+int(stoptime/(4*lam)),p=F2simu)]

print("finished initialisation")























#class for a read. this stores the read sequence (ref), start position (ps) and direction (direction) of the fragment
class Read:
  def __init__(self, ref, pos, chr, direction):
    self.ref = ref
    self.pos = pos
    self.chr = chr
    self.direction = direction




if diploid:
	genotypesSplit=np.zeros((lenG,2), dtype=np.dtype('uint8'))
	genotypesSplit[0,0]=0
	genotypesSplit[0,1]=0
	genotypesSplit[1,0]=0
	genotypesSplit[1,1]=1
	genotypesSplit[2,0]=0
	genotypesSplit[2,1]=2
	genotypesSplit[3,0]=0
	genotypesSplit[3,1]=3
	genotypesSplit[4,0]=1
	genotypesSplit[4,1]=1
	genotypesSplit[5,0]=1
	genotypesSplit[5,1]=2
	genotypesSplit[6,0]=1
	genotypesSplit[6,1]=3
	genotypesSplit[7,0]=2
	genotypesSplit[7,1]=2
	genotypesSplit[8,0]=2
	genotypesSplit[8,1]=3
	genotypesSplit[9,0]=3
	genotypesSplit[9,1]=3
	if (delRatio>=0.000000000001 and theta>=0.000000000001) or errD>=0.000000000001:
		genotypesSplit[10,0]=0
		genotypesSplit[10,1]=4
		genotypesSplit[11,0]=1
		genotypesSplit[11,1]=4
		genotypesSplit[12,0]=2
		genotypesSplit[12,1]=4
		genotypesSplit[13,0]=3
		genotypesSplit[13,1]=4
		genotypesSplit[14,0]=4
		genotypesSplit[14,1]=4

#Sample a haploid sequence from a diploid genome sequence
def sampleHaplo(seq):
	haploChoice=np.random.choice(2,size=len(seq))
	seqHaplo=np.where(haploChoice==0,genotypesSplit[seq,0],genotypesSplit[seq,1])
	return seqHaplo

# substitution sequencing error probability distribution. I assume here nucleotides are interchangeable. This is used for simulating reads.
probDistrErr=np.zeros(4)
probDistrErr[0]=1-e
probDistrErr[1:4]=e/3
			
#sequencing error probabilities for a base that is a deletion (wrt the reference) and could be mis-read into any of the 4 nucleotides
gapProbDist=np.zeros(5)
gapProbDist[4]=1-errWeird
for i in range(4):
	gapProbDist[i]=errWeird/4
	
# probability distribution sequencing error of a nucleotide that could be mis-read into a gap
delProbDist=np.zeros(2)
delProbDist[0]=1-errD
delProbDist[1]=errD




#New version: this function creates a batch of read, with length sampled from L and position from F or F2.
def createReads(L,Fsimu,F2simu,strat,batchSize,not_ended):
	rangeBatch=np.arange(batchSize)

	#read direction
	if reversereads:
	  direction = np.random.choice(2,size=batchSize)
	else:
	  direction = np.zeros(batchSize)
	  
	#number of forwards and reverse reads to simulate
	reverses=np.sum(direction)
	forwards=batchSize-reverses
	#sample read positions
	forwardPoss=np.random.choice(N,size=forwards,p=Fsimu)
	reversePoss=np.random.choice(N,size=reverses,p=F2simu)
	  	
	#find chromosomes of the reads
	chr=np.full(batchSize,chNum-1)
	for c in range2N[:chNum]:
		chr[:forwards]=np.where(forwardPoss<chCumsum[chNum-c],chNum-c-1,chr[:forwards])
		chr[forwards:]=np.where(reversePoss<chCumsum[chNum-c],chNum-c-1,chr[forwards:])

	#sample read lengths
	lengths = np.random.choice(lastl, p=L[:lastl], size=batchSize)+1
	#print("Read start "+str(start_pos) +" direction "+str(direction)+" length "+str(length)+" sequence ")
	
	if linear:
		lengths[:forwards]=np.where(forwardPoss+lengths[:forwards]>chCumsum[chr[:forwards]+1],chCumsum[chr[:forwards]+1]-forwardPoss,lengths[:forwards])
		lengths[forwards:]=np.where(reversePoss+1-lengths[forwards:]<chCumsum[chr[forwards:]],reversePoss+1-chCumsum[chr[forwards:]],lengths[forwards:])
	timeToNaive=np.sum(lengths)+alpha*batchSize
	timeToRU=alpha*batchSize
	
	#arrays containing the total coverage of the new batch for naive and RU strategies
	coverageToAdd=np.zeros((N,lenB),dtype=np.dtype('uint32'))
	coverageToAddRU=np.zeros((N,lenB),dtype=np.dtype('uint32'))
	
	#simulate read sequences read by read and add to batch coverage - first forward reads
	for i in rangeBatch[:forwards]:
		rLen=lengths[i]
		rChr=chr[i]
		startPos=forwardPoss[i]
		if not not_ended:
			if not strat[0][startPos]:
				if rLen>mu:
					rLen=mu
		doubleRead=False
		#find corresponding genome sequence for the read
		if theta<0.000000000001:
			seq1=np.zeros(rLen, dtype=np.dtype('uint8'))
		else:
			if startPos+rLen<=chCumsum[rChr+1]:
				seq1=genome[startPos:startPos+rLen]
			else:
				doubleRead=True
				seq1=genome[startPos:chCumsum[rChr+1]]
				seq2=genome[chCumsum[rChr]:chCumsum[rChr]+startPos+rLen-chCumsum[rChr+1]]
			if diploid:
				seq1=sampleHaplo(seq1)
				if doubleRead:
					seq2=sampleHaplo(seq2)
		#add sequencing error to the read
		seq1=simulateErrors(seq1)
		if doubleRead:
			seq2=simulateErrors(seq2)
		#add read to coverage for naive strategy
		rLen1=len(seq1)
		reshapen=seq1.reshape((rLen1,1))
		np.put_along_axis(coverageToAdd[startPos:startPos+rLen1], reshapen, np.take_along_axis(coverageToAdd[startPos:startPos+rLen1], reshapen, axis=1)+1, axis=1)
		if doubleRead:
			rLen2=len(seq2)
			reshapen2=seq2.reshape((rLen2,1))
			np.put_along_axis(coverageToAdd[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, np.take_along_axis(coverageToAdd[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, axis=1)+1, axis=1)
		#add read to coverage for RU strategy
		if strat[0][startPos]:
			timeToRU+=rLen
			np.put_along_axis(coverageToAddRU[startPos:startPos+rLen1], reshapen, np.take_along_axis(coverageToAddRU[startPos:startPos+rLen1], reshapen, axis=1)+1, axis=1)
			if doubleRead:
				np.put_along_axis(coverageToAddRU[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, np.take_along_axis(coverageToAddRU[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, axis=1)+1, axis=1)
		else:
			timeToRU+=rho+mu
			if rLen1>=mu:
				rLen1=mu
				seq1=seq1[:mu]
				reshapen=seq1.reshape((mu,1))
			np.put_along_axis(coverageToAddRU[startPos:startPos+rLen1], reshapen, np.take_along_axis(coverageToAddRU[startPos:startPos+rLen1], reshapen, axis=1)+1, axis=1)
			if rLen1<mu and doubleRead:
				rLen2=min(rLen2,mu-rLen1)
				seq2=seq2[:rLen2]
				reshapen2=seq2.reshape((rLen2,1))
				np.put_along_axis(coverageToAddRU[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, np.take_along_axis(coverageToAddRU[chCumsum[rChr]:chCumsum[rChr]+rLen2], reshapen2, axis=1)+1, axis=1)
		
	#now reverse reads
	for i in rangeBatch[:reverses]:
		rLen=lengths[forwards+i]
		rChr=chr[forwards+i]
		startPos=reversePoss[i]
		if not not_ended:
			if not strat[1][startPos]:
				if rLen>mu:
					rLen=mu
		doubleRead=False
		#find corresponding genome sequence for the read
		if theta<0.000000000001:
			seq1=np.zeros(rLen, dtype=np.dtype('uint8'))
		else:
			if startPos+1-rLen>=chCumsum[rChr]:
					seq1=genome[startPos+1-rLen:startPos+1]
			else:
					doubleRead=True
					seq2=genome[startPos+1-rLen-chCumsum[rChr]+chCumsum[rChr+1]:chCumsum[rChr+1]]
					seq1=genome[chCumsum[rChr]:startPos+1]
			if diploid:
				seq1=sampleHaplo(seq1)
				if doubleRead:
					seq2=sampleHaplo(seq2)
		#add sequencing error to the read
		seq1=simulateErrors(seq1)
		if doubleRead:
			seq2=simulateErrors(seq2)
		#add read to coverage for naive strategy
		rLen1=len(seq1)
		reshapen=seq1.reshape((rLen1,1))
		np.put_along_axis(coverageToAdd[startPos+1-rLen1:startPos+1], reshapen, np.take_along_axis(coverageToAdd[startPos+1-rLen1:startPos+1], reshapen, axis=1)+1, axis=1)
		if doubleRead:
			rLen2=len(seq2)
			reshapen2=seq2.reshape((rLen2,1))
			np.put_along_axis(coverageToAdd[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, np.take_along_axis(coverageToAdd[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, axis=1)+1, axis=1)
		#add read to coverage for RU strategy
		if strat[1][startPos]:
			timeToRU+=rLen
			np.put_along_axis(coverageToAddRU[startPos+1-rLen1:startPos+1], reshapen, np.take_along_axis(coverageToAddRU[startPos+1-rLen1:startPos+1], reshapen, axis=1)+1, axis=1)
			if doubleRead:
				np.put_along_axis(coverageToAddRU[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, np.take_along_axis(coverageToAddRU[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, axis=1)+1, axis=1)
		else:
			timeToRU+=rho+mu
			if rLen1>=mu:
				seq1=seq1[rLen1-mu:]
				rLen1=mu
				reshapen=seq1.reshape((mu,1))
			np.put_along_axis(coverageToAddRU[startPos+1-rLen1:startPos+1], reshapen, np.take_along_axis(coverageToAddRU[startPos+1-rLen1:startPos+1], reshapen, axis=1)+1, axis=1)
			if rLen1<mu and doubleRead:
				rLen2new=min(rLen2,mu-rLen1)
				seq2=seq2[rLen2-rLen2new:rLen2]
				rLen2=rLen2new
				reshapen2=seq2.reshape((rLen2,1))
				np.put_along_axis(coverageToAddRU[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, np.take_along_axis(coverageToAddRU[chCumsum[rChr+1]-rLen2:chCumsum[rChr+1]], reshapen2, axis=1)+1, axis=1)
	
	return coverageToAdd, coverageToAddRU, timeToNaive, timeToRU



	
def simulateErrors(seq):
	# if there is sequencing error, add it to the read sequence
	if e>0.000000000001:
			#simulate substitution errors
			substErrs=np.random.choice(4,size=len(seq), p=probDistrErr)
			newSeq=np.where(seq!=4,(seq+substErrs)%4,seq)
			seq_old=newSeq
			#simulate gap sequencing errors
			if errD>0.000000000001:
				delErrs=np.random.choice(2,size=len(seq), p=delProbDist)
				newSeq=np.where(delErrs==1,4,newSeq)
			# add errors in sequencing gap positions
			if theta>0.000000000001 and delRatio>0.000000000001 and errWeird>0.000000000001 :
				gapErrs=np.random.choice(5,size=len(seq), p=gapProbDist)
				newSeq=np.where(seq_old==4,gapErrs,newSeq)
	return newSeq






#This function creates a single read, with length sampled from L and position from F or F2. Now not used anymore
def createRead(L,Fsimu,F2simu):
	#read direction
	if reversereads:
	  direction = np.random.choice(2)
	else:
	  direction = 0
	  
	# sampling read starting position
	if direction==0:
	  if covRatio>1.000000000001:
	    readPosSampled[0]+=1
	    if readPosSampled[0]>len(readPoss):
	    	newReadPoss=np.random.choice(N,size=10+int(stoptime/(10*lam)),p=Fsimu)
	    	readPoss.extend(newReadPoss.tolist())
	    start_pos=readPoss[readPosSampled[0]-1]
	  else:
	  	start_pos = np.random.choice(N)
	else:
	  if covRatio>1.000000000001:
	    readPosSampled2[0]+=1
	    if readPosSampled2[0]>len(readPoss2):
	    	newReadPoss=np.random.choice(N,size=10+int(stoptime/(10*lam)),p=F2simu)
	    	readPoss2.extend(newReadPoss.tolist())
	    start_pos=readPoss2[readPosSampled2[0]-1]
	  else:
	  	start_pos = np.random.choice(N)
	  	
	#find chromosome of the read
	chr=chNum-1
	for c in range2N[:chNum]:
		if start_pos<chCumsum[c+1]:
			chr=c
			break

	#sampling read length
	length = np.random.choice(lastl, p=L[:lastl])+1
	#print("Read start "+str(start_pos) +" direction "+str(direction)+" length "+str(length)+" sequence ")
	
	if linear:
		if direction==0 and (start_pos+length>chCumsum[chr+1]):
			length=chCumsum[chr+1]-start_pos
		elif direction==1 and (start_pos+1-length<chCumsum[chr]):
			length=start_pos+1-chCumsum[chr]
	
	if theta<0.000000000001:# read sequence by default is made of zero, because the true genome is made of all A's
		seq=np.zeros(length, dtype=np.dtype('uint8'))
	else: #if there is a non-reference sequenced genome, extract its portion covered by the read
			if direction==0:
				if start_pos+length<=chCumsum[chr+1]:
					seq=genome[start_pos:start_pos+length]
				else:
					seq=np.concatenate((genome[start_pos:chCumsum[chr+1]],genome[chCumsum[chr]:chCumsum[chr]+start_pos+length-chCumsum[chr+1]]))
			else:
				if start_pos+1-length>=chCumsum[chr]:
					seq=np.flip(genome[start_pos+1-length:start_pos+1])
				else:
					seq=np.flip(np.concatenate((genome[start_pos+1-length-chCumsum[chr]+chCumsum[chr+1]:chCumsum[chr+1]],genome[chCumsum[chr]:start_pos+1])))
			if diploid:
				seq=sampleHaplo(seq)
	
	# if there is sequencing error, add it to the read sequence
	if e>0.000000000001:
			#simulate substitution errors
			substErrs=np.random.choice(4,size=length, p=probDistrErr)
			seq=np.where(seq!=4,(seq+substErrs)%4,seq)
			seq_old=seq
			#simulate gap sequencing errors
			if errD>0.000000000001:
				delErrs=np.random.choice(2,size=length, p=delProbDist)
				seq=np.where(delErrs==1,4,seq)
			# add errors in sequencing gap positions
			if theta>0.000000000001 and delRatio>0.000000000001 and errWeird>0.000000000001 :
				gapErrs=np.random.choice(5,size=length, p=gapProbDist)
				seq=np.where(seq_old==4,gapErrs,seq)
			
	read=Read(seq, start_pos, chr, direction)
	return read



	


#this initialises the prior (which is \pi in the manuscript section 0.1.1) 
#and the scores (the big chunk of calculations (equation 5) in Section 0.1.1, and similarly in 0.1.2).
def init_priors_and_scores():
	priors=np.zeros((1,lenG))
	if theta<0.000000000001:
		print("init_priors to the case the reference is not known (theta=0)")
		prior_distr = np.full((N,lenG),1.0/lenG,dtype=np.dtype('float64'))
		priors.fill(1.0/lenG)
	else:
		print("init_priors to an unknown genome with diversity theta from the reference genome which is made only of A's (theta>0)")
		if theta*(1.0+delRatio)>1.0:
			print("PROBLEM: theta="+str(theta)+" (with indels "+str(theta*(1.0+delRatio))+") too high or indel rate too high, almost all genome expected to be different from the reference")
			exit()
		#priors are all identical across the genome, so we create it for one position and then repeat it
		priors[0][0]=1.0-(theta*(1.0+delRatio))
		if not diploid:
			for b in range(3):
				priors[0][b+1]=theta/3
			if delRatio>0.000000000001:
				priors[0][4]=theta*delRatio
		else:
			for b in range(3):
				priors[0][b+1]=(1.0-propHomo)*theta/3
			priors[0][4]=(propHomo)*theta/3
			priors[0][7]=(propHomo)*theta/3
			priors[0][9]=(propHomo)*theta/3
			if delRatio>0.000000000001:
				priors[0][10]=(1.0-propHomo)*theta*delRatio
				priors[0][14]=(propHomo)*theta*delRatio
		prior_distr=np.repeat(priors, N, axis=0)
		
	#Now initialize the scores
	score=np.zeros(1)
	sitewise_score_read(score,priors)
	scores=np.repeat(score, N, axis=0)

	#create regions interesting to sequence, if required.
	#by setting the prior over non-interesting position to 1 0 0 0 0, it means we already know they are all A's, which means we have no advantage in sequencing them.
	if nLoci>0.001:
		spacing=int(N/nLoci)
		for i in range(nLoci):
			regions.append([i*spacing,i*spacing+lenLoci])
			for b in range(lenG):
				prior_distr[i*spacing+lenLoci:(i+1)*spacing,b]=np.where(genome[i*spacing+lenLoci:(i+1)*spacing]==b,1.0,0.0)
			scores[i*spacing+lenLoci:(i+1)*spacing]=0.0
	
	return prior_distr,scores

if 2*N<batchSize:
	range2N=np.arange(2*batchSize)
phi_stored=np.full((lenB,lenG,batchSize),1.0)
for i in range(lenB):
	for j in range(lenG):
		phi_stored[i,j,:]=phi[i,j]**range2N[:batchSize]

def update_post_genome(posterior, coverageToAdd):
	#here we create an array with position-specific probabilities (dependent on the base read at the given position)
	covs=[]
	indexing=[]
	for i in range(lenB):
		indexing.append(coverageToAdd[:,i]>0)
		covs.append(coverageToAdd[indexing[i],i])
	phis=np.full(N,1.0)
	for j in range(lenG):
		if j>0:
			phis.fill(1.0)
		for i in range(lenB):
			phis[indexing[i]]*=phi_stored[i,j,covs[i]]
		posterior[:,j]*=phis
	#sumB is Z_i(D) in manuscript section 0.1.1 and is the normalisation factor for the posterior (the likelihood of the new data at the position).
	sumB=np.sum(posterior,axis=1)
	posterior /= sumB[:, np.newaxis]
	return
	

  
#update the posteriors given one read (now not used anymore)
def update_post_1read(posterior, read):
	lenP=len(posterior)
	#here we create an array with position-specific probabilities (dependent on the base read at the given position)
	phis=np.zeros((lenP,lenG))
	for j in range(lenG):
			phis[:,j]=phi[read,j]
	posterior*=phis
    #sumB is Z_i(D) in manuscript section 0.1.1 and is the normalisation factor for the posterior (the likelihood of the new data at the position).
	sumB=np.sum(posterior,axis=1)
	for i in range(lenG):
		posterior[:,i] /= sumB
		posterior[:,i]=np.where(np.isfinite(posterior[:,i]),posterior[:,i],0.0)
	return



#this takes a read and updates an array (count) containing coverage per position.
def update_count_1read(count,read):
	rLen=len(read.ref)
	rPos=read.pos
	ch=read.chr
	sc=chCumsum[ch]
	ec=chCumsum[ch+1]
	if read.direction == 0:
		reshapen=read.ref.reshape((rLen,1))
		if linear or rPos+rLen<=ec:
			np.put_along_axis(count[rPos:rPos+rLen], reshapen, np.take_along_axis(count[rPos:rPos+rLen], reshapen, axis=1)+1, axis=1)
		else:
			np.put_along_axis(count[rPos:ec], reshapen[:ec-rPos], np.take_along_axis(count[rPos:ec], reshapen[:ec-rPos], axis=1)+1, axis=1)
			np.put_along_axis(count[sc:sc+rLen-ec+rPos], reshapen[ec-rPos:], np.take_along_axis(count[sc:sc+rLen-ec+rPos], reshapen[ec-rPos:], axis=1)+1, axis=1)
			#np.put_along_axis(count[read.pos:N], reshapen[:N-read.pos], np.take_along_axis(count[read.pos:N], reshapen[:N-read.pos,:], axis=1)+1, axis=1)
			#np.put_along_axis(count[:rLen-N+read.pos], reshapen[N-read.pos:], np.take_along_axis(count[:rLen-N+read.pos], reshapen[N-read.pos:], axis=1)+1, axis=1)
	else:
		reshapen=np.flip(read.ref).reshape((rLen,1))
		if linear or rPos-rLen>sc-2:
			np.put_along_axis(count[rPos-rLen+1:rPos+1], reshapen, np.take_along_axis(count[rPos-rLen+1:rPos+1], reshapen, axis=1)+1, axis=1)
		else:
			np.put_along_axis(count[sc:rPos+1], reshapen[rLen-rPos+sc-1:], np.take_along_axis(count[sc:rPos+1], reshapen[rLen-rPos+sc-1:], axis=1)+1, axis=1)
			np.put_along_axis(count[ec-(rLen-rPos+sc-1):ec], reshapen[:rLen-rPos+sc-1], np.take_along_axis(count[ec-(rLen-rPos+sc-1):ec], reshapen[:rLen-rPos+sc-1], axis=1)+1, axis=1)
	return


#timeApprox=[0.0]

##finds S(i) at points covered in a certain area (either covered by a read or over the whole genome, depending on the length of the input arrays)
#this uses the current posterior probabilities to find the score of a given position.
#This is the big chunk of calculations (equation 5) in Section 0.1.1, and similarly in 0.1.2
#How to make this even faster? Maybe one way could be to do these operations only for positions where probabilities are not too small
# compared to the largest one in the genome (but ignoring probabilities >0.5).
def sitewise_score_read(scores,pos_posterior):
	lenS=len(scores)
	#Shannon entropy of the current posterior distribution. "entropy" is part of the calculation of S(i) in section 0.1.1
	logs=np.log(pos_posterior,where=pos_posterior>0.0)
	entropy=np.sum(-pos_posterior*logs,axis=1)
	#Expected entropy after a new batch of reads ("newEntropy").
	#this is also part of the calculation of S(i) in section 0.1.1 (part of the final line of the big series of equations)
	newEntropy=np.zeros(lenS)
	#probability of observing a new base of each type at the considered position
	#these observation_probabilities are the P(d_{n+1,i}) currently defined in equation (6) in section0.1.1
	observation_probabilities=np.zeros(lenS)
	#posterior probabilities after reading a certain base i
	#these are the f_i(b|D') calculated as in the last line in equation (4) of section 0.1.1
	newPost=np.zeros((lenS,lenG))
	for i in range(lenB): # new read base
		np.multiply(pos_posterior,phi[i],out=newPost)
		np.sum(newPost,axis=1,out=observation_probabilities)
		newPost /= observation_probabilities[:, np.newaxis]
		np.log(newPost,where=newPost>0.0,out=logs)
		for j in range(lenG): # genotype
			newEntropy-=(observation_probabilities*newPost[:,j]*logs[:,j])
	#update scores
	scores[:]=entropy-newEntropy
	return



#Approximate version - only calculates the score of positions with non-negligeable scores. This is faster and is recommended unless time is not an issue
def sitewise_score_read_approximate(scores,allPosteriors):
	values=np.where(allPosteriors>0.5,0.0,allPosteriors)
	best=np.max(values)
	values=np.where(values>best/thresholdApproximation,values,0.0)
	values=np.sum(values,axis=1)
	indeces=np.nonzero(values)[0] #indeces of genome positions with non-negligeable scores
	pos_posterior=allPosteriors[indeces,:]
	
	lenS=len(indeces)
	#Shannon entropy of the current posterior distribution. "entropy" is part of the calculation of S(i) in section 0.1.1
	logs=np.log(pos_posterior,where=pos_posterior>0.0)
	entropy=np.sum(-pos_posterior*logs,axis=1)
	#Expected entropy after a new batch of reads ("newEntropy").
	#this is also part of the calculation of S(i) in section 0.1.1 (part of the final line of the big series of equations)
	newEntropy=np.zeros(lenS)
	#probability of observing a new base of each type at the considered position
	#these observation_probabilities are the P(d_{n+1,i}) currently defined in equation (6) in section0.1.1
	observation_probabilities=np.zeros(lenS)
	#posterior probabilities after reading a certain base i
	#these are the f_i(b|D') calculated as in the last line in equation (4) of section 0.1.1
	newPost=np.zeros((lenS,lenG))
	for i in range(lenB): # new read base
		np.multiply(pos_posterior,phi[i],out=newPost)
		np.sum(newPost,axis=1,out=observation_probabilities)
		newPost /= observation_probabilities[:, np.newaxis]
		np.log(newPost,where=newPost>0.0,out=logs)
		for j in range(lenG): # genotype
			newEntropy-=(observation_probabilities*newPost[:,j]*logs[:,j])
	#update scores
	scores.fill(0.0)
	scores[indeces]=entropy-newEntropy
	return




#this is S^{\mu}(i,o) in section 2.2, that is, the sum of the scores S for mu positions starting from any position i.
#Note that there are 2 arrays of length N: one is for forward reads (index 0) and one is for backward reads (index 1).
#The array is filled in a dynamic programming sort of way.
def update_S_mu(S_mu,S):
  #New version with numpy.cumsum, more efficient!
  for ch in range2N[:chNum]:
  	#forward reads
  	S_mu[chCumsum[ch]][0]=np.sum(S[chCumsum[ch]:chCumsum[ch]+mu])
  	toBeSummed=np.zeros(2*chArray[ch])
  	toBeSummed[0]=S_mu[chCumsum[ch]][0]
  	toBeSummed[2:2*chArray[ch]-2*mu+2:2]=S[chCumsum[ch]+mu:chCumsum[ch+1]]
  	toBeSummed[3::2]=-S[chCumsum[ch]:chCumsum[ch+1]-1]
  	if not linear:
  		toBeSummed[2*chArray[ch]-2*mu+2::2]=S[chCumsum[ch]:chCumsum[ch]+mu-1]
  	partialSums=np.cumsum(toBeSummed)
  	S_mu[chCumsum[ch]+1:chCumsum[ch+1],0]=partialSums[3::2]
  	#reverse reads
  	#toBeSummed=np.zeros(2*chArray[ch])
  	toBeSummed.fill(0)
  	toBeSummed[::2]=S[chCumsum[ch]:chCumsum[ch+1]]
  	toBeSummed[2*mu+1::2]=-S[chCumsum[ch]:chCumsum[ch+1]-mu]
  	if not linear:
  		toBeSummed[0]+=np.sum(S[chCumsum[ch+1]-mu+1:chCumsum[ch+1]])
  		toBeSummed[3:2*mu+1:2]=-S[chCumsum[ch+1]-mu+1:chCumsum[ch+1]]
  	partialSums=np.cumsum(toBeSummed)
  	S_mu[chCumsum[ch]:chCumsum[ch+1],1]=partialSums[1::2]
  return





# Given S and S_mu, here we find the optimal strategy for rejecting fragments.
#see section 0.2.2
#the output is a pair of boolean arrays, one saying which forward reads to accept (depending on their starting position) and one for the reverse reads.
def find_strat(S,S_mu):
  #Sdivided are the scores divided by the number of partitions - 1
  #it doesn't have an interpretation in the manuscript, it's a trick to save on the number of operations done.
  Sdivided=S/(partitionNum-1)
  
  #U[i] is the expected benefit of a read that starts at position i (counting from 0 on).
  #see a description in section 0.1.3 and efficient algorithm in section 0.1.4, and how to use it in section 0.2.2 
  #U[0][1] is the expected benefit of a reverse fragment whose first base maps on the first position of the genome.
  #U[i][0] is the benefit of a forward fragment starting at position i (first genome position is 0).
  U=np.zeros((N,2),dtype=np.dtype('float64'))
  addenda=len(dummyPart)+1
  for ch in range2N[:chNum]:
  	sc=chCumsum[ch]
  	ec=chCumsum[ch+1]
  	U[sc][0]=np.sum(S[sc:sc+dummyPart[0]])
  	for bd in range(len(dummyPart)-1):
  		U[sc][0] +=np.sum(Sdivided[sc+dummyPart[bd]:sc+dummyPart[bd+1]])*(partitionNum-2-bd)
  	U[sc][1]=S[sc]
  	if not linear:
  		U[sc][1]+=np.sum(S[ec-dummyPart[0]+1:ec])
  		for bd in range(len(dummyPart)-1):
  			U[sc][1] +=np.sum(Sdivided[ec-dummyPart[bd+1]+1:ec-dummyPart[bd]+1])*(partitionNum-2-bd)
  	
  	# forward benefits
  	toBeAdded=np.zeros(chArray[ch]*addenda+1)
  	toBeAdded[0]=U[sc][0]
  	toBeAdded[1::addenda]=-S[sc:ec]
  	for bdI in range(addenda-1):
  		bd=dummyPart[bdI]
  		toBeAdded[2+bdI:(chArray[ch]-bd)*addenda+2+bdI:addenda]=Sdivided[sc+bd:ec]
  		if not linear:
  			toBeAdded[(chArray[ch]-bd)*addenda+2+bdI::addenda]=Sdivided[sc:sc+bd]
  	partialSums=np.cumsum(toBeAdded)
  	U[sc+1:ec,0]=partialSums[addenda:chArray[ch]*addenda:addenda]
  	
  	# reverse benefits
  	toBeAdded.fill(0)
  	toBeAdded[0]=U[sc][1]
  	toBeAdded[1:(chArray[ch]-1)*addenda:addenda]=S[sc+1:ec]
  	for bdI in range(addenda-1):
  		bd=dummyPart[bdI]
  		toBeAdded[(bd-1)*addenda+2+bdI::addenda]=-Sdivided[sc:ec-bd+1]
  		if not linear:
  			toBeAdded[2+bdI:(bd-1)*addenda+2+bdI:addenda]=-Sdivided[ec-bd+1:ec]
  	partialSums=np.cumsum(toBeAdded)
  	U[sc+1:ec,1]=partialSums[addenda:chArray[ch]*addenda:addenda]
          
  # u[i] is the u_i defined in Supplementary Section 2.
  #values of u up to index N-1 are for forward fragments, from N to 2N-1 are for backward fragments.
  #u[i] is the expected benefit increase obtained by keeping reading a fragment mapping at i instead of rejecting it.
  #u[i]<0 can only happen if mu>dummyPart[0] (fragment piece to be mapped longer than the minimum read length), which we don't allow, meaning we neglect the contribution of very short reads when deciding a strategy (however note that a strategy is not needed for deciding about such short reads).
  #if u[i] is negative and t[i] is positive, fragments mapping at i should be rejected (however with the new version of the code u[i]<0 should not happen).
  u=np.append(Fhalf*(U[:,0]-S_mu[:,0]),F2half*(U[:,1]-S_mu[:,1]))
    
  #vector of ratios
  uot=u/t
  #if time cost is negative (or almost zero), always accept.  
  uot=np.where(t<=0.000000000001,np.inf,uot)
  #argsort the u/t array
  # "I" contains the list of genome positions from the most valuable for the strategy to the least valuable.
  #values of u and t up to index N-1 are for forward fragments, from N to 2N-1 are for backward fragments.
  reversed_I=np.argsort(uot)
  I = reversed_I[::-1]
  
  # average benefit of strategy, initialized to the case that all fragments are rejected 
  #(like U^0 at the end of Supplementary Section 2).
  Ubar0=np.sum(Fhalf*S_mu[:,0])+np.sum(F2half*S_mu[:,1])
  #average time cost of strategy, initialized to the case that all fragments are rejected.
  #(like T^0 at the end of Supplementary Section 2).
  Tbar = T0
    
  # this is more operations than the old version, but by using numpy functions it should be almost always faster.
  #Find the best strategy (or equivalently, the number of positions stratSize to accept).
  #here we start with a starategy that rejects all fragments and increase its size one by one.
  #in some scenario it might be faster to start from a strategy that accepts everything and then remove fragments one by one.
  cumsumU= np.cumsum(u[I])+Ubar0
  cumsumT= np.cumsum(t[I])+T0
  #stratSize is the total number of accepted positions (the number of 1's) of the current strategy 
  stratSize=np.argmax(cumsumU/cumsumT)+1
  
  #now we write down the strategy as 2 boolean arrays (one for forward reads and one for backward reads)
  #forwardI[i]=False means that a forward read starting at position i (first genome position is 0) has to be rejected.
  strat=np.zeros(2*N,dtype=bool)
  np.put(strat,I[:stratSize],True)
  forwardI = strat[:N]
  reverseI = strat[N:]
  print("Accepting "+str(np.sum(forwardI))+" forward fragments, "+str(np.sum(reverseI))+" reverse fragments")
  return [forwardI,reverseI]



#update posteriors and scores given a total base count of a batch of reads over the genome
def updatePostAndS_genome(coverageToAdd,S,post_distr):
	update_post_genome(post_distr, coverageToAdd)
	if approximate:
		sitewise_score_read_approximate(S,post_distr)
	else:
		sitewise_score_read(S,post_distr)


#update posteriors and scores given a read, now not used anymore
def updatePostAndS(r,S,post_distr):
		lenR=len(r.ref)#read length
		ch=r.chr #chromosome of the read
		sc=chCumsum[ch]#beginning of read chromosome
		ec=chCumsum[ch+1]#end of read chromosome
		doubleUp=False #Does the read pass across the end of the circular genome?
		if r.direction ==0:
			start1=r.pos #First position of the genome to be updated
			end2=r.pos+lenR #Last position of the genome to be updated
			if start1+lenR>ec:
				end2=start1+lenR-chArray[ch]
				doubleUp=True
				seq1=r.ref[:ec-start1]
				seq2=r.ref[ec-start1:]
			else:
				seq1=r.ref
		else:
			end2=r.pos+1
			start1=r.pos-lenR+1
			if r.pos-lenR+1<sc:
				start1=r.pos-lenR+1+chArray[ch]
				doubleUp=True
				seq1=np.flip(r.ref[lenR-(ec-start1):])
				seq2=np.flip(r.ref[:lenR-(ec-start1)])
			else:
				seq1=np.flip(r.ref)
		if doubleUp: # the read passes across the end of the circular genome, so it needs to be considered as two reads when updating
				update_post_1read(post_distr[start1:ec], seq1)
				update_post_1read(post_distr[sc:end2], seq2)
				sitewise_score_read(S[start1:ec],post_distr[start1:ec])
				sitewise_score_read(S[sc:end2],post_distr[sc:end2])
		else: #update tracks of scores and benefits for a plain read
				update_post_1read(post_distr[start1:end2], seq1)
				sitewise_score_read(S[start1:end2],post_distr[start1:end2])



























print("=========================Simulating reads, and naive and read until strategy============================")

if testing:
	start = time.time()

print("Initializing priors and scores")
prior_distr, S = init_priors_and_scores()
print("Prior and scores initialized")
print(prior_distr)
post_distr= prior_distr
# The array of scores of individual chromosome positions (equation 5 in section 0.1.1) for naive strategy
print(S)

#used for recording values
reshapenGenome=genome.reshape((N,1))			
  
# posteriors and scores for read until strategy
post_distrRU= np.copy(prior_distr)
SRU=np.copy(S)

#maximum benefit gain achievable
totalbenefit = np.sum(S)
print("total benefit is ",totalbenefit)
  
print("Initializing S_mu and first strategy before any reads")
if testing:
	timeUpdateSnaive+=time.time()-start
	start = time.time()
S_mu = np.zeros((N,2),dtype=np.dtype('float64'))
update_S_mu(S_mu,S)
if testing:
          timeUpdateSmu+=time.time()-start
          start = time.time()
strat = find_strat(S,S_mu) #strat[0] is for forward reads, strat[1] if for reverse reads
if testing:
          timeUpdateStrategy+=time.time()-start

t1=0 #counts of reads so far for naive strategy
t3=0 #number of steps in plotting (how many times we have recorded things so far)
t2=0 #counts of reads so far for RU strategy
t4=0 #number of steps in plotting (how many times we have recorded things so far) for RU
t5=1 #number of strategy updates done so far


#list that will contain the reads simulated if such reads have to be saved to file
newReads = []

#total time (in sequencing units) so far for each simulation (naive and read until)
timeNaive = 0
timeRU=0
# total benefit gained across the genome from the sequencing process, in information theory units, for naive and RU strategies
benefits = 0
benefitsRU=0
#coverage of each position of the genome
count = np.zeros((N,lenB),dtype=np.dtype('uint32'))
countRU = np.zeros((N,lenB),dtype=np.dtype('uint32'))
# tracks which reads have been accepted and which rejected
readaccepted = []
#storing here some values for plotting, second list is naive sequencing, first list is read until
counttimes = [[],[]]
# if args.savecounts:
# 	savecount = [[],[]]
savebasetime = [[],[]]
savebenefits = [[],[]]
basecertaintysum = [[],[]]
probTruthLists = [[],[]]
errProbs = [[],[]]
aveCoverage = [[],[]]
minCoverage = [[],[]]
stratSize=[[],[]]
runTime=[]

#if false terminate naive strategy simulations (the naive strategy will almost certainly use fewer reads than the RU strategy)
not_ended = True

#: an additional criterion for stopping simulations when reaching a certain benefit threshold - I personally would not use it.
#if byPercent:
#  endbenefits = totalbenefit*stoppercent





def storeValues(isNaive,timeNaive,S,post_distr,count,CPtimeUpdating):
	#counttimes[isNaive].append(t2)
	savebasetime[isNaive].append(timeNaive)
	benefits=np.sum(S)
	savebenefits[isNaive].append(benefits)
	post_distr_noTruth=np.copy(post_distr)
	np.put_along_axis(post_distr_noTruth, reshapenGenome, 0.0, axis=1)
	errProb=np.sum(post_distr_noTruth)
	errProbs[isNaive].append(errProb)
	if nLoci>0.001:
		aveCov=0.0
		minCovL=np.zeros(nLoci)
		for reg in range(nLoci):
			sumArray=np.sum(count[regions[reg][0]:regions[reg][1]],axis=1)
			aveCov+=np.sum(sumArray)/N
			minCovL[reg]=np.min(sumArray)
		minCov=np.min(minCovL)
	else:
		sumArray=np.sum(count,axis=1)
		aveCov=np.sum(sumArray)/N
		minCov=np.min(sumArray)
	aveCoverage[isNaive].append(aveCov)
	minCoverage[isNaive].append(minCov)
	print("isNaive "+str(isNaive)+" stop time "+str(timeNaive)+"   coverage "+str(aveCov)+" minimum coverage: "+str(minCov)+"   benefits left "+str(benefits)+" expected errors: "+str(errProb))
	basecertaintysum[isNaive].append(np.sum(np.amax(post_distr,axis=1)))
	probTruthLists[isNaive].append(np.mean(np.choose(genome, post_distr.T)))
	if isNaive==0:
		stratSize[isNaive].append(np.sum(strat[0]*(Fsimu*0.5)) + np.sum(strat[1]*(F2simu*0.5)))
		runTime.append(CPtimeUpdating)
	else:
		stratSize[isNaive].append(1.0)

batchNumber=0
start = time.time()

timeFor1stUpdate=args.timeFor1stUpdate
strategyEvery=args.strategyEvery
timeWaitedRU=0.0
timeWaitedNaive=0.0
coverageToAdd=np.zeros((N,lenB),dtype=np.dtype('uint32'))
coverageToAddRU=np.zeros((N,lenB),dtype=np.dtype('uint32'))
CPtimeUpdating=0.0

storeValues(0,timeRU,SRU,post_distrRU,countRU,CPtimeUpdating)
storeValues(1,timeNaive,S,post_distr,count,CPtimeUpdating) 
if testing:
		timeStoring+= time.time()-start
		start = time.time()

while ( not_ended or timeRU<=stoptime):
    #now create a new read
	coverageToAddpartial, coverageToAddRUpartial , timeToNaive , timeToRU = createReads(L,Fsimu,F2simu,strat,batchSize,not_ended)
	if testing:
		timeGenerateReads+= time.time()-start
		start = time.time()
	if not_ended : #now update the results of the naive strategy
		#new (sequencing) time spent after sequencing the read batch
		timeNaive += timeToNaive
		timeWaitedNaive+=timeToNaive
		# updating coverage
		count+=coverageToAddpartial
		coverageToAdd+=coverageToAddpartial
		not_ended = timeNaive<=stoptime
	
	#now RU strategy: update scores, posteriors, coverage, and time of the sequencing run
	timeRU+=timeToRU
	timeWaitedRU+=timeToRU
	countRU+=coverageToAddRUpartial
	coverageToAddRU+=coverageToAddRUpartial
	if (not updateOnlyWhenNecessary) or (timeRU>=timeFor1stUpdate and timeWaitedRU>strategyEvery):
    	# update the posteriors and the benefit scores
		updatePostAndS_genome(coverageToAddRU,SRU,post_distrRU)
		coverageToAddRU.fill(0)
		timeUpdateNow1=time.time()-start
		if testing:
			timeUpdateS+=timeUpdateNow1
			start = time.time()
		 #If sufficient sequencing time has passed, we update the strategy (note that simulated sequencing time is 
		 #completely different from the time spent on the computer running these simulations)
		if timeRU>=timeFor1stUpdate and timeWaitedRU>strategyEvery:
			timeWaitedRU=0.0
			update_S_mu(S_mu,SRU)
			strat = find_strat(SRU,S_mu)
			timeUpdateNow2=time.time()-start
			CPtimeUpdating+=timeUpdateNow1+timeUpdateNow2
			if testing:
				timeUpdateStrategy+=timeUpdateNow2
				start = time.time()
		#save values for plots
		storeValues(0,timeRU,SRU,post_distrRU,countRU,CPtimeUpdating)
		if testing:
			timeStoring+= time.time()-start
			start = time.time()
		
		if not_ended :
			#update the posteriors and the scores (used only to compare the efficiency of the two strategies, 
			#and not for the naive strategy which doesn't need any of these information)
			updatePostAndS_genome(coverageToAdd,S,post_distr)
			coverageToAdd.fill(0)
			if testing:
				timeUpdateSnaive+=time.time()-start
				start = time.time()
			timeWaitedNaive=0.0
			#save values for plots
		storeValues(1,timeNaive,S,post_distr,count,None) 
		if testing:
				timeStoring+= time.time()-start
				start = time.time()
		




#Old simulations: not doing this in batches
# countpartsize = 1000000
# readspert = 4000000
# while False and ( not_ended or timeRU<=stoptime):
#     if testing:
#       start = time.time()
#     if not_ended and (t3*countpartsize<=timeNaive): #from time to time here we record values of the simulations for later making plots
#       counttimes[1].append(t2)
#       # if args.savecounts:
# # 	      savecount[1].append(np.copy(count))
#       savebasetime[1].append(timeNaive)
#       benefits=np.sum(S)
#       savebenefits[1].append(benefits)
#       
#       post_distr_noTruth=np.copy(post_distr)
#       np.put_along_axis(post_distr_noTruth, reshapenGenome, 0.0, axis=1)
#       errProb=np.sum(post_distr_noTruth)
#       errProbs[1].append(errProb)
#       if nLoci>0.001:
#       	aveCov=0.0
#       	minCovL=np.zeros(nLoci)
#       	for reg in range(nLoci):
#       		sumArray=np.sum(count[regions[reg][0]:regions[reg][1]],axis=1)
#       		aveCov+=np.sum(sumArray)/N
#       		minCovL[reg]=np.min(sumArray)
#       	minCov=np.min(minCovL)
#       else:
#       	sumArray=np.sum(count,axis=1)
#       	aveCov=np.sum(sumArray)/N
#       	minCov=np.min(sumArray)
#       aveCoverage[1].append(aveCov)
#       minCoverage[1].append(minCov)
#       print("naive stop "+str(t3)+"   time "+str(timeNaive)+"   coverage "+str(aveCov)+" minimum coverage: "+str(minCov)+"   benefits left "+str(benefits)+" expected errors: "+str(errProb))
#       basecertaintysum[1].append(np.sum(np.amax(post_distr,axis=1)))
#       probTruthLists[1].append(np.mean(np.choose(genome, post_distr.T)))
#       stratSize[1].append(len(strat))
#       t3+=1
#     if t4*countpartsize<=timeRU: #again, here we record values for the RU strategy
#       counttimes[0].append(t1)
#       # if args.savecounts:
# # 	      savecount[0].append(np.copy(countRU))
#       savebasetime[0].append(timeRU)
#       benefitsRU=np.sum(SRU)
#       savebenefits[0].append(benefitsRU)
#       
#       post_distr_noTruth=np.copy(post_distrRU)
#       np.put_along_axis(post_distr_noTruth, reshapenGenome, 0.0, axis=1)
#       errProb=np.sum(post_distr_noTruth)
#       errProbs[0].append(errProb)
#       if nLoci>0.001:
#       	aveCov=0.0
#       	minCovL=np.zeros(nLoci)
#       	for reg in range(nLoci):
#       		sumArray=np.sum(countRU[regions[reg][0]:regions[reg][1]],axis=1)
#       		aveCov+=np.sum(sumArray)/N
#       		minCovL[reg]=np.min(sumArray)
#       		#aveCov+=np.sum(countRU[regions[reg][0]:regions[reg][1],:])/N
#       		#minCovL[reg]=np.min(np.sum(countRU[regions[reg][0]:regions[reg][1],:],axis=1))
#       	minCov=np.min(minCovL)
#       else:
#       	sumArray=np.sum(countRU,axis=1)
#       	aveCov=np.sum(sumArray)/N
#       	minCov=np.min(sumArray)
#       aveCoverage[0].append(aveCov)
#       minCoverage[0].append(minCov)
#       print("RU stop "+str(t4)+" time "+str(timeRU)+" coverage "+str(aveCov)+" minimum coverage: "+str(minCov)+"   benefits left "+str(benefitsRU)+" expected errors: "+str(errProb))
#       basecertaintysum[0].append(np.sum(np.amax(post_distrRU,axis=1)))
#       probTruthLists[0].append(np.mean(np.choose(genome, post_distrRU.T)))
#       stratSize[0].append(np.sum(strat))
#       t4+=1
#     if testing:
#       timeStoring+= time.time()-start
#       start = time.time()
#     #now create a new read
#     r=createRead(L,F,F2)
#     if args.savereads:
#     	newReads.append(r)
#     if testing:
#       timeGenerateReads+= time.time()-start
#     if not_ended: #now update the results of the naive strategy
# 	    #new (sequencing) time spent after sequencing the read by the naive strategy
# 	    timeNaive += len(r.ref) + alpha
# 	    # updating coverage
# 	    if testing:
# 	      start = time.time()
# 	    update_count_1read(count,r)
# 	    if testing:
# 	      timeUpdateCountNaive+=time.time()-start
# 	      start = time.time()
# 		# given a read and its direction, we update the posteriors and the scores (used only to compare the efficiency of the two strategies, and not for the naive strategy which doesn't need any of these information)
# 	    #print("naive")
# 	    updatePostAndS(r,S,post_distr)
# 		
# 	    if testing:
# 	      timeUpdateSnaive+=time.time()-start
# 	    t2+=1
# 	    if byPercent:
# 	      not_ended = benefits<=endbenefits
# 	      if not notEnded:
# 	      	stoptime = timeNaive
# 	    else:
# 	      not_ended = timeNaive<=stoptime
#     
#     #now RU strategy: update scores, posteriors, coverage, and time of the sequencing run
#     accept=strat[r.direction][r.pos]
#     readaccepted.append(accept)
#     if testing:
#       start = time.time()
#     if accept or len(r.ref)<mu:
#        	truncatedRead=r
#        	effectiveLen=len(r.ref) #in cases in which mu is larger than read length we surely don't reject the read.
#     else: #if the read has to be rejected, its length becomes mu, and we create a new truncatedRead
#           	effectiveLen=mu
#           	timeRU += rho
#           	truncatedRead=Read(r.ref[:effectiveLen],r.pos,r.chr,r.direction)
#     timeRU+=effectiveLen + alpha
#     if testing:
#       timeGenerateReads+= time.time()-start
#       start = time.time()
#     update_count_1read(countRU,truncatedRead)
#     if testing:
# 	      timeUpdateCount+=time.time()-start
# 	      start = time.time()
#     
#     #given a read and its direction, we update the posteriors and the scores
#     #print("RU")
#     updatePostAndS(truncatedRead,SRU,post_distrRU)
#             
#     if testing:
# 	      timeUpdateS+=time.time()-start
# 	      start = time.time()
#     
#     t1+=1
#     #If sufficient sequencing time has passed, we update the strategy (note that simulated sequencing time is completely different from the time spent on the computer running these simulations)
#     if t5 * readspert <=timeRU:
#         update_S_mu(S_mu,SRU)
#         if testing:
#           timeUpdateSmu+=time.time()-start
#           start = time.time()
#         strat = find_strat(SRU,S_mu)
#         if testing:
#           timeUpdateStrategy+=time.time()-start
#         #print("Reads processed: "+str(t1)+"   max score "+str(max(SRU))+"    time "+str(timeRU))
#         t5+=1
# 
# if False:
# 	if testing:
# 			  start = time.time()
# 	#record the final values
# 	counttimes[1].append(t2)
# 	# if args.savecounts:
# 	# 	savecount[1].append(count)
# 	savebasetime[1].append(timeNaive)
# 	benefits=np.sum(S)
# 	savebenefits[1].append(benefits)
# 	post_distr_noTruth=np.copy(post_distr)
# 	np.put_along_axis(post_distr_noTruth, reshapenGenome, 0.0, axis=1)
# 	errProb=np.sum(post_distr_noTruth)
# 	errProbs[1].append(errProb)
# 	if nLoci>0.001:
# 		aveCov=0.0
# 		minCovL=np.zeros(nLoci)
# 		for reg in range(nLoci):
# 			aveCov+=np.sum(count[regions[reg][0]:regions[reg][1],:])/N
# 			minCovL[reg]=np.min(np.sum(count[regions[reg][0]:regions[reg][1],:],axis=1))
# 		minCov=np.min(minCovL)
# 	else:
# 		aveCov=np.sum(np.sum(count))/N
# 		minCov=np.min(np.sum(count,axis=1))
# 	#aveCov=np.sum(np.sum(count))/N
# 	aveCoverage[1].append(aveCov)
# 	#minCov=np.min(np.sum(count,axis=1))
# 	minCoverage[1].append(minCov)
# 	basecertaintysum[1].append(np.sum(np.amax(post_distr,axis=1)))
# 	probTruthLists[1].append(np.mean(np.choose(genome, post_distr.T)))
# 	stratSize[1].append(len(strat))
# 
# 	counttimes[0].append(t1)
# 	# if args.savecounts:
# 	# 	savecount[0].append(countRU)
# 	savebasetime[0].append(timeRU)
# 	benefitsRU=np.sum(SRU)
# 	savebenefits[0].append(benefitsRU)
# 	post_distr_noTruth=np.copy(post_distrRU)
# 	np.put_along_axis(post_distr_noTruth, reshapenGenome, 0.0, axis=1)
# 	errProb=np.sum(post_distr_noTruth)
# 	errProbs[0].append(errProb)
# 	if nLoci>0.001:
# 		aveCov=0.0
# 		minCovL=np.zeros(nLoci)
# 		for reg in range(nLoci):
# 			aveCov+=np.sum(countRU[regions[reg][0]:regions[reg][1],:])/N
# 			minCovL[reg]=np.min(np.sum(countRU[regions[reg][0]:regions[reg][1],:],axis=1))
# 		minCov=np.min(minCovL)
# 	else:
# 		aveCov=np.sum(np.sum(countRU))/N
# 		minCov=np.min(np.sum(countRU,axis=1))
# 	#aveCov=np.sum(np.sum(countRU))/N
# 	aveCoverage[0].append(aveCov)
# 	#minCov=np.min(np.sum(countRU,axis=1))
# 	minCoverage[0].append(minCov)
# 	basecertaintysum[0].append(np.sum(np.amax(post_distrRU,axis=1)))
# 	probTruthLists[0].append(np.mean(np.choose(genome, post_distrRU.T)))
# 	stratSize[0].append(np.sum(strat))
# 
# 









print("=========================Save to file===============================")



if not os.path.exists("simulationsOutput"):
	os.makedirs("simulationsOutput")


simulationFileString="simulationsOutput/N"+str(N)+"_theta"+str(theta)
if covRatio>1.000001:
	simulationFileString=simulationFileString+"_nPeaks"+str(nPeaks)+"_covRatio"+str(covRatio)
if nLoci>0:
	simulationFileString=simulationFileString+"_nLoci"+str(nLoci)+"_lenLoci"+str(lenLoci)
if linear:
	simulationFileString=simulationFileString+"_linear"
if diploid:
	simulationFileString=simulationFileString+"_diploid"
simulationFileString=simulationFileString+"_batchSize"+str(batchSize)+"_timeFor1stUpdate"+str(timeFor1stUpdate)+"_strategyEvery"+str(strategyEvery)+"_run"+str(args.runID)
#data0 = list(zip([N],[totalbenefit],[min(len(counttimes[0]),len(counttimes[1]))]))
#df0 = pd.DataFrame(data = data0, columns = ['N','totalbenefit','frames'])
#if args.runID==1:
#  df0.to_csv(simulationFileString+'_info.csv',index=False)

if args.saveFinalCounts:
	shortening=1000
	if nLoci>0.001:
		filePos=open(simulationFileString+'_interestingRegions.csv',"w")
		spacing=int(N/nLoci)
		for i in range(nLoci):
			for b in range(int(lenLoci/shortening)):
				filePos.write("1,")
			for b in range(int((spacing-lenLoci)/shortening)):
				filePos.write("0,")
		filePos.write("\n")
		filePos.close()
	filePos=open(simulationFileString+'_coverageNaive.csv',"w")
	for i in range(int(N/shortening)):
		filePos.write(str(np.sum(count[shortening*i:shortening*(i+1),:])/shortening)+",")
	filePos.write("\n")
	filePos.close()
	filePos=open(simulationFileString+'_coverageRU.csv',"w")
	for i in range(int(N/shortening)):
		filePos.write(str(np.sum(countRU[shortening*i:shortening*(i+1),:])/shortening)+",")
	filePos.write("\n")
	filePos.close()		

# if args.savecounts:
# 	countdata = []
# 	for i in range(N):
# 	  for t in range(len(counttimes[0])):
# 	    countdata.append([i,savecount[0][t][i][basedict['A']],savecount[0][t][i][basedict['C']],savecount[0][t][i][basedict['G']],savecount[0][t][i][basedict['T']],0,savebasetime[0][t]])
# 	  for t in range(len(counttimes[1])):
# 	    countdata.append([i,savecount[1][t][i][basedict['A']],savecount[1][t][i][basedict['C']],savecount[1][t][i][basedict['G']],savecount[1][t][i][basedict['T']],1,savebasetime[1][t]])
# 
# 
# 	countdf = pd.DataFrame(data = countdata, columns = ['position','A','C','G','T','strategy','time'])
# 	if args.runID==1:
# 	  countdf.to_csv(simulationFileString+'_counts.csv',index=False)

distrdata = []
counttime=len(minCoverage[0])
print(minCoverage)
print(runTime)
Nlist=[N]*counttime
distrdata.extend(list(zip(Nlist,basecertaintysum[1],probTruthLists[1],errProbs[1],aveCoverage[1],minCoverage[1],savebenefits[1],stratSize[1],savebasetime[1][:counttime],basecertaintysum[0],probTruthLists[0],errProbs[0],aveCoverage[0],minCoverage[0],savebenefits[0],stratSize[0],runTime,savebasetime[0][:counttime])))
dfdistr = pd.DataFrame(data = distrdata, columns = ['N','Certaintysum','ProbTruth','errProb','aveCoverage','minCoverage','benefits','accptingProbNaive','Time','CertaintysumRU','ProbTruthRU','errProbRU','aveCoverageRU','minCoverageRU','benefitsRU','accptingProbRU','computationalTimeRU','TimeRU'])
dfdistr.to_csv(simulationFileString+'_distr.csv',index=False)

# if args.savereads:
# 	endpos = []
# 	for r in newReads:
# 	  if r.direction==0:
# 	    endpos.append(r.pos+len(r.ref)+1)
# 	  else:
# 	    endpos.append(r.pos-len(r.ref)+1)
# 
# 	readdata = list(zip([r.pos for r in newReads],endpos,[len(r.ref) for r in newReads],[r.ref for r in newReads],[r.direction for r in newReads],readaccepted))
# 	dfreads = pd.DataFrame(data = readdata, columns=['Position', 'End Position','Length','String','Direction','Accepted?'])
# 	dfreads.to_csv(simulationFileString+'_reads.csv',index=False)


#print to screen time taken for each task
if testing:
	timeLogging+=time.time()-start
	timeTotal=time.time()-startTotal
	print("total time: "+str(timeTotal))
	print("time logging: "+str(timeLogging))
	print("time storing: "+str(timeStoring))
	print("time generating reads: "+str(timeGenerateReads))
	print("time updating S and posteriors, naive strategy: "+str(timeUpdateSnaive))
	print("time updating S and posteriors, RU strategy: "+str(timeUpdateS))
	#print("time updating counts naive strategy: "+str(timeUpdateCountNaive))
	#print("time updating counts RU strategy: "+str(timeUpdateCount))
	#print("time updating S_mu: "+str(timeUpdateSmu))
	print("time updating strategy: "+str(timeUpdateStrategy))

exit()