# ReadUntilStrategy

script simulateReadUntil.py is used to run simulations of sequencing and Read Until.
The file also contains the functions defining our strategy.

Script makePlots.y was used to plot the results from the simulations.

Typical usage command lines are:

python simulateReadUntil.py -N 4000000 -runID 1  -stoptime 200000000 -batchSize 4000 -timeFor1stUpdate 1 -strategyEvery 4000000  -saveFinalCounts -approximate

python makePlots.py -file /Desktop/ReadUntil_nanopore/simulations/simulationsOutput/N4000000_mu500_rho500.0_alpha300.0_lam3000.0_sd6000.0_theta0.01_delRatio0.4_e0.06_errD0.03_errWeird0.1_nPeaks20.0_covRatio1.0_nLoci0_lenLoci500.0